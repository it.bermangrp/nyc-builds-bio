<!DOCTYPE html>
<html lang="en" class="<?php echo returnBrowser(); ?>">
    <head>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135906261-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-135906261-1');
      </script>


      <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Launched in 2018, NYC Builds Bio+ connects commercial life science opportunities to New York City’s real estate development community through events, research, reports and educational programs." />
        <title><?php bloginfo('name');?></title>
        <link rel="canonical" href="http://www.nycbuildsbio.org" />

        <?php wp_head(); ?>

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,600,700,800,900" rel="stylesheet">
        <link rel='stylesheet' type='text/css' href='<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.all.css' />
        <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/magnific/magnific.css">
        <link rel='stylesheet' type='text/css' href='<?php echo PAGEDIR; ?>/machines/libraries/leaflet/leaflet.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo PAGEDIR; ?>/machines/libraries/slick/slick.css' />
        <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/magnific/magnific.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/styles/styles.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body data-tempdir="<?php echo PAGEDIR; ?>" id="<?php echo get_post( $post )->post_name; ?>" <?=(returnMobile() == 'true')? 'class="mobileMode"' : 'class="desktopMode"';?>>

        <!-- POPULATE JAVASCRIPT VARIABLES -->
            <div>
                <?php
                    generatePagesJSON(get_the_ID());
                    populateJavascript(returnMobile(), 'mobile');

                    if(DEVMODE){
                        populateJavascript('true', 'dev_mode');
                    } else {
                        populateJavascript('false', 'dev_mode');
                    }

                    if(isset($_GET['url'])){
                        populateJavascript('true', 'dev_refresh');
                    } else {
                        populateJavascript('false', 'dev_refresh');
                    }

                    if ( is_user_logged_in() ) {
                        populateJavascript('true', 'user_logged_in');

                        $userdat = wp_get_current_user();
                        $allUserData = listUsers('array');

                        foreach ($allUserData as $key => $value) {
                            if($value['id'] == $userdat->data->ID){
                                $userData = $value;
                            }
                        }

                        populateJSON($userData, 'user_data');
                    } else {
                        populateJavascript('false', 'user_logged_in');
                    }
                ?>
            </div>

        <!-- HEADER -->
            <nav class="navbar navbar-default navbar-fixed-top header">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo home_url(); ?>">
                            <img src="<?php echo PAGEDIR; ?>/images/graphics/logo.png" alt="<?php bloginfo('name');?>" />
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="menu_social_container">
                        <div class="social-header">
                            <a class="hyperlink" target="_blank" href="https://www.facebook.com/NYCBuildsBio/">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            <a class="hyperlink" target="_blank" href="https://twitter.com/nycbuildsbio?lang=en">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a class="hyperlink" target="_blank" href="https://www.instagram.com/nycbuildsbio/?hl=en">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                            <a class="hyperlink" target="_blank" href="https://www.linkedin.com/company/nyc-builds-bio/">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <?php

                        class bootstrapDropDown extends Walker_Nav_Menu {
                            function start_lvl( &$output, $depth = 0, $args = array() ) {
                                $indent = str_repeat("\t", $depth);
                                $output .= "\n$indent<ul class=\"sub-menu dropdown-menu\">\n";
                            }
                        }

                        $defaults = array(
                            'theme_location'  => 'header-menu',
                            'menu'            => '',
                            'container'       => 'div',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id'    => 'navbar',
                            'menu_class'      => 'nav navbar-nav',
                            'menu_id'         => '',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '<span>',
                            'link_after'      => '</span>',
                            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth'           => 0,
                            'walker'          => new bootstrapDropDown()
                            );
                        wp_nav_menu( $defaults );

                    ?>
                </div>
            </nav>

        <!-- PAGE CONTENT -->
            <section class="pageSection"></section>

        <!-- FOOTER -->
            <footer class="footer">
                <div class="container">
                    <div class="footer-info">
                        <div class="logo-footer">
                            <a class="navbar-brand" href="<?php echo home_url(); ?>">
                                <img src="<?php echo PAGEDIR; ?>/images/graphics/logo_white.png" alt="<?php bloginfo('name');?>" />
                            </a>
                        </div>
                        <h6 class="contact-info">
                            <span>380 Lexington Ave</span> <span>Suite 1920</span> <span>New York, NY 10168</span> <span><a class="hyperlink" href="tel:+12124507300">212.450.7300</a></span> <span><a href="mailto:info@nycbuildsbio.org">info@nycbuildsbio.org</a></span>
                        </h6>
                        <div class="social-footer">
                            <a class="hyperlink" target="_blank" href="https://www.facebook.com/NYCBuildsBio/">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                            <a class="hyperlink" target="_blank" href="https://twitter.com/nycbuildsbio?lang=en">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                            <a class="hyperlink" target="_blank" href="https://www.instagram.com/nycbuildsbio/?hl=en">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                            <a class="hyperlink" target="_blank" href="https://www.linkedin.com/company/nyc-builds-bio/">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- SCRIPTS -->
            <div>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/modernizr/modernizr.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/boilerplate/boilerplate.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/magnific/magnific.min.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/slick/slick.min.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/validate/validate.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.min.all.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/leaflet/leaflet.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/sticky/jquery.sticky.js"></script>
                <?php if (returnBrowser() !== "internet-explorer-8"): ?>
                    <script src="<?php echo PAGEDIR; ?>/machines/libraries/dynamics/dynamics.js"></script>
                <?php endif; ?>
                <?php wp_footer(); ?>
            </div>
    </body>
</html>
