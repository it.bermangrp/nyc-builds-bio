<div class="person_item_bio">
	<div class="container">
		<div class="person_item_bio_container">
			<div class="person_item_info">
				<h3 class="person_name"><span class="person_first_name"></span> <span class="person_last_name"></span></h3>
				<h4 class="person_work_title"></h4>
				<h4 class="person_company"></h4>
			</div>
			<div class="person_item_bio">
				<div class="the_content"></div>
			</div>
		</div>
	</div>
</div>