<div class="map">
	<div class="container">
		<div class="map_content_container">
			<div class="map_container">
			<div class="mapouter">
    		<div class="gmap_canvas">
				<iframe height="400" width="300" border="0" marginwidth="0" marginheight="0" src="https://www.mapquest.com/embed/us/ny/new-york/10168-0002/380-lexington-ave-suite-1505-40.751104,-73.976334?center=40.751224000000015,-73.97662999999999&zoom=15&maptype=map"></iframe>        <style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}</style>
					<style>.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style>
				</div>
			</div>
			<style>iframe {width:100%;height:100%;}</style>

<!-- 
				<div id="mapid"></div>
				<div class="mapBG"></div> -->
			</div>


			<div class="contact">
				<div class="contact_info_header">
					<h3>NYC Builds Bio+</h3>
				</div>
				<div class="contact_info">
					<p><a href="mailto:info@nycbuildsbio.org">info@nycbuildsbio.org</a></p>
				</div>
				<div class="contact_info">
					<p><a class="hyperlink" href="tel:+12124507300">212.450.7300</a></p>
				</div>
				<div class="contact_info">
					<p>380 Lexington Avenue, Suite 1505<br />New York, NY 10168</p>
				</div>

				<div class="contact_info">
					<p><a class="hyperlink" target="_blank" href="https://nycbuildsbio.us19.list-manage.com/subscribe?u=85c1b947d47d9cd7d63fbaf52&id=acfcc98cb4"><strong>Click here </strong></a>to subscribe to our mailing list  </p>
				</div>
			</div>
		</div>
	</div>
</div>