<li class="event_calendar_item">
	<div class="event_calendar_container">
		<div class="event_day_container">
			<h1 class="event_day"></h1>
			<h4 class="event_day_of_week"></h4>
		</div>
		<div class="event_time_title">
			<h5 class="event_time"><span class="event_start"></span> - <span class="event_end"></span></h5>
			<p class="the_title"></p>
		</div>
	</div>
</li>