<div class="page_events_item">
	<div class="banner_image_container"></div>
	<div class="container">
		<div class="sidebar_and_content_container">
			<div class="stickySidebar">
				<div class="sticky_sidebar">
					<ul class="sidebar_content">
						<a href="" class="about sidebar_button"><li>About the Program</li></a>
						<a href="" class="agenda sidebar_button"><li>Agenda</li></a>
						<a href="" class="videos sidebar_button"><li>Videos</li></a>
						<a href="" class="gallery sidebar_button"><li>Image Gallery</li></a>
						<!-- <a href="" class="speakers sidebar_button"><li>Speakers</li></a> -->
						<a href="" class="sponsors sidebar_button"><li>Sponsors</li></a>
						<a href="http://www.nycbuildsbio.org/wp-content/uploads/2020/12/NYC-2020-Symposium-Digital-Program.pdf" target="_blank" class="register sidebar_button displayNone"><li>Digital Program</li></a>

						<a href="https://www.youtube.com/playlist?list=PLgsXkaZ2C0y0QW05U3IyWDRlnktkuTmsa" target="_blank" class="register sidebar_button displayNone"><li>Day 1 – <br/>Event Recap Videos</li></a>
						<a href="https://www.youtube.com/playlist?list=PLgsXkaZ2C0y281EeV6boX1iNa6exk5I6E" target="_blank" class="register sidebar_button displayNone"><li>Day 2 – <br/>Event Recap Videos</li></a>
						<a href="https://www.youtube.com/playlist?list=PLgsXkaZ2C0y2UIklxcLwOcDLPjgM5JaCi" target="_blank" class="register sidebar_button displayNone"><li>Day 3 – <br/>Event Recap Videos</li></a>

					</ul>
				</div>
			</div>
			<div class="content_container"></div>
		</div>
	</div>
</div>