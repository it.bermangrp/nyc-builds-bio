<li class="month_container">
	<div class="month_block">
		<div class="month_nav_block">
			<div class="prev_button_container button_container"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
			<h3 class="month_title_container"></h3>
			<div class="next_button_container button_container"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
		</div>
		<ul class="event_listing_container"></ul>
		<div class="month_nav_block">
			<div class="prev_button_container button_container"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
			<h3 class="month_title_container"></h3>
			<div class="next_button_container button_container"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
		</div>
	</div>
</li>