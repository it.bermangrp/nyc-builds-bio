<div class="testimonial_item">
	<div class="container">
		<div class="content_block">
			<div class="empty_container"></div>
			<div class="person_block">
				<div class="image_block"></div>
				<div class="the_content"></div>
			</div>
			<div class="quote_block">
				<div class="double_quotes top_quote">
					<img src="https://www.nycbuildsbio.org/wp-content/themes/nycbuildsbio/images/graphics/double-quotes.png" alt="double quotes" />
				</div>
				<div class="test_editor2"></div>
				<div class="double_quotes bottom_quote">
					<img src="https://www.nycbuildsbio.org/wp-content/themes/nycbuildsbio/images/graphics/double-quotes.png" alt="double quotes" />
				</div>
			</div>
		</div>
	</div>
</div>
