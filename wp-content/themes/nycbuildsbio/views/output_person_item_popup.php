<div class="person_item_popup">
	<div class="popup_left_column">
		<div class="person_headshot"></div>
		<div class="name_block">
			<h4 class="person_first_name"></h4> <h4 class="person_last_name"></h4>
		</div>
		<div class="person_title">
			<h5 class="person_work_title"></h5>
			<h5 class="person_company"></h5>
		</div>
	</div>
	<div class="popup_right_column">
		<div class="content_container">
			<div class="the_content"></div>
		</div>
	</div>
</div>