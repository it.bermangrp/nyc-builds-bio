<div class="past_event_item">
	<div class="image_container"><img src="" alt="" class="image"></div>
	<div class="info_container">
		<div class="event_date_info">
			<h5 class="event_day"></h5>
		</div>
		<h3 class="the_title"></h3>
		<!-- <a href="" class="event_item_link">Event Recap</a> -->
		<div class="rsvp"><a href="" class="button event_item_link">Event Recap</a></div>
		<!-- <div class="source_container">
			<strong><a class="ni_source"></a></strong>
		</div> -->
	</div>
</div>