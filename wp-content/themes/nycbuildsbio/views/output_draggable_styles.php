<style>
	.ui-state-default{
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}

	.draggableContainer_<?php echo $draggableSettings['classID']; ?>{
		width: <?php echo $draggableSettings['parentContainerWidth']; ?>px;
		height: <?php echo $draggableSettings['parentContainerHeight']; ?>px;
		border: 1px solid #333;
	}

	.draggableContainer_<?php echo $draggableSettings['classID']; ?> .draggable{
		width: 100%;
		height: 100%;
	}

	.draggableContainer_<?php echo $draggableSettings['classID']; ?> .draggable .ui-draggable{
		width: <?php echo $draggableSettings['itemWidth']; ?>px;
		height: <?php echo $draggableSettings['itemHeight']; ?>px;
	}

</style>