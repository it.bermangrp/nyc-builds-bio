<div class="formWrapper">
	<form class="commentForm" method="get" action="">
		<fieldset>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="individual_name">Individual's Name:</label>
					<input type="text" class="individual_name" name="individual_name" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="job_title">Job Title:</label>
					<input type="text" class="job_title" name="job_title" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="company_name">Company Name:</label>
					<input type="text" class="company_name" name="company_name" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="company_address">Company Address:</label>
					<input type="text" class="company_address" name="company_address" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="email">Email Address:</label>
					<input type="text" class="email" name="email" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="office_phone">Office Phone:</label>
					<input type="text" class="office_phone" name="office_phone" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="cell_phone">Cell Phone:</label>
					<input type="text" class="cell_phone" name="cell_phone" />
				</div>
			</div>
			<div class="formField multi_checkbox">
				<div class="fieldContent">
					<fieldset>
					    <h4>Organization or Company Type</h4>
					    <p>I am a(n):</p>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="real_estate_company" name="real_estate_company" value="real_estate_company" />
					        <label for="real_estate_company">Real Estate Company</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="life_science_company" name="life_science_company" value="life_science_company" />
					        <label for="life_science_company">Life Science Company</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="institution" name="institution" value="institution" />
					        <label for="institution">Institution</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="consultant_or_service_provider" name="consultant_or_service_provider" value="consultant_or_service_provider" />
					        <label for="consultant_or_service_provider">Consultant or Service Provider</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="investor" name="investor" value="investor" />
					        <label for="investor">Investor</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="public_agency" name="public_agency" value="public_agency" />
					        <label for="public_agency">Public Agency</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="other" name="other" value="other" />
					        <label for="other">Other</label>
					    </div>
					</fieldset>
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent text_field">
					<label for="elaborate">Please elaborate:</label>
					<textarea name="elaborate" id="elaborate" class="elaborate" cols="30" rows="5"></textarea>
				</div>
			</div>

<!-- 			<div class="formField">
				<div class="fieldContent text_field">
					<label for="more_information">I am interested in the following type of membership or would like more information regarding:</label>
					<textarea name="more_information" id="more_information" class="more_information" cols="30" rows="5"></textarea>
				</div>
			</div> -->
			<div class="formField multi_checkbox">
				<div class="fieldContent">
					<fieldset>
				
					    <p>I am interested in the following type of membership</p>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Individual_Real_Estate_Membership" name="Individual_Real_Estate_Membership" value="Individual_Real_Estate_Membership" />
					        <label for="Individual_Real_Estate_Membership">Individual Real Estate Membership ($500)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Individual_Government_Membership" name="Individual_Government_Membership" value="Individual Government Membership" />
					        <label for="Individual_Government_Membership">Individual Government Membership ($250)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Individual_Life_Science_Membership" name="Individual_Life_Science_Membership" value="Individual_Life_Science_Membership" />
					        <label for="Individual_Life_Science_Membership">Individual Life Science Membership ($250)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Student_Membership" name="Student_Membership" value="Student_Membership" />
					        <label for="Student_Membership">Student Membership ($150)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Corporate_Associate" name="Corporate_Associate" value="Corporate_Associate" />
					        <label for="Corporate_Associate">Corporate Associate ($5,000)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Corporate_Premium" name="Corporate_Premium" value="Corporate_Premium" />
					        <label for="Corporate_Premium">Corporate Premium ($10.000)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Elite_Member" name="Elite_Member" value="Elite_Member" />
					        <label for="Elite_Member">Elite Member ($15,000)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Signature_Member" name="Signature_Member" value="Signature_Member" />
					        <label for="Signature_Member">Signature Member ($20,000)</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Life_Science_Membership" name="Life_Science_Membership" value="Life_Science_Membership" />
					        <label for="Life_Science_Membership">Life Science Membership</label>
					    </div>
					    <div class="checkbox_flex">
					        <input type="checkbox" id="Institutional_Public_Memberships" name="Institutional_Public_Memberships" value="Institutional_Public_Memberships" />
					        <label for="Institutional_Public_Memberships">Institutional & Public Memberships ($1,500)</label>
					    </div>
					</fieldset>
				</div>
			</div>


			<div class="formField captchaField" id="sampleFormCaptcha"></div>
			<div class="formField submit_container">
				<input class="submit button" type="submit" value="Submit"/>
			</div>
		</fieldset>
		<div class="formResponse"></div>
	</form>
</div>