<div class="event_item">
	<div class="event_item_link">
		<div class="event_container">
			<div class="image_container"></div>
			<div class="event_info">
				<p class="the_title"></p>
				<br />
				<div class="event_date_info">
					<h5 class="event_day"></h5>
					<h5 class="event_time"></h5>
				</div>
				<br />
			</div>
			<div class="event_details">
				<div class="location_content"></div>
			</div>
			<div class="rsvp register_button displayNone"><a href="" class="button">Register</a></div>
			<div class="rsvp learn_more displayNone"><a href="" class="button event_item_link">Learn More</a></div>
		</div>
	</div>
</div>