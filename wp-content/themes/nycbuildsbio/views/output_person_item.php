<div class="person_item">
	<div class="person_block">
		<div class="person_wrapper">
			<div class="image_container">
				<div class="person_image"></div>
			</div>
			<div class="person_info">
				<h4 class="person_name"><span class="person_first_name"></span> <span class="person_last_name"></span></h4>
				<h5 class="person_company"></h5>
			</div>
			<div class="bio_button"><a href="" class="button">Read Bio</a></div>
		</div>
	</div>
</div>