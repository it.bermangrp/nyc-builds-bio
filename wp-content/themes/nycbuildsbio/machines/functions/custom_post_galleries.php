<?php

// REGISTER CUSTOM POST TYPE
	add_action( 'init', 'register_post_type_galleries');
	function register_post_type_galleries(){

		$labels = array(
			'name' => 'Galleries',
			'singular_name' => 'Gallery',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Gallery',
			'edit_item' => 'Edit Gallery',
			'new_item' => 'New Gallery',
			'view_item' => 'View Gallery',
			'search_items' => 'Search Galleries',
			'not_found' => 'Nothing found',
			'not_found_in_trash' => 'Nothing found in trash',
			'parent_item_colon' => ''
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title')
		);

		register_post_type( 'galleries', $args);

	}

// DEFINE META BOXES
	$galleriesMetaBoxArray = array(
	    "galleries_gallery_image_array_meta" => array(
	    	"id" => "galleries_gallery_image_array_meta",
	        "name" => "Gallery Image Array",
	        "post_type" => "galleries",
	        "position" => "side",
	        "priority" => "low",
	        "callback_args" => array(
	        	"input_type" => "input_hidden",
	        	"input_name" => "galleries_gallery_image_array"
	        )
	    ),
	    "galleries_gallery_images_meta" => array(
	    	"id" => "galleries_gallery_images_meta",
	        "name" => "Gallery Images",
	        "post_type" => "galleries",
	        "position" => "side",
	        "priority" => "low",
	        "callback_args" => array(
	        	"input_type" => "input_blank",
	        	"input_name" => "galleries_gallery_images"
	        )
	    ),
	    "galleries_all_images_meta" => array(
	    	"id" => "galleries_all_images_meta",
	        "name" => "All Images",
	        "post_type" => "galleries",
	        "position" => "normal",
	        "priority" => "low",
	        "callback_args" => array(
	        	"input_type" => "input_blank",
	        	"input_name" => "galleries_all_images"
	        )
	    ),
	);

// ADD META BOXES
	add_action( "admin_init", "admin_init_galleries" );
	function admin_init_galleries(){
		global $galleriesMetaBoxArray;
		generateMetaBoxes($galleriesMetaBoxArray);
	}

// SAVE POST TO DATABASE
	add_action('save_post', 'save_galleries');
	function save_galleries($thisID){
		global $galleriesMetaBoxArray;
		savePostData($galleriesMetaBoxArray, $thisID, $wpdb);
	}

// SORTING CUSTOM SUBMENU

	add_action('admin_menu', 'register_sortable_galleries_submenu');

	function register_sortable_galleries_submenu() {
		add_submenu_page('edit.php?post_type=galleries', 'Sort Galleries', 'Sort', 'edit_pages', 'galleries_sort', 'sort_galleries');
	}

	function sort_galleries() {
		
		echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
			echo '<h2>Sort Galleries</h2>';
		echo '</div>';

		listGalleries('sort');
	}

// CUSTOM COLUMNS

	// add_action("manage_posts_custom_column",  "galleries_custom_columns");
	// add_filter("manage_edit-galleries_columns", "galleries_edit_columns");

	// function galleries_edit_columns($columns){
	// 	$columns = array(
	// 		"full_name" => "Gallery Name",
	// 	);

	// 	return $columns;
	// }
	// function galleries_custom_columns($column){
	// 	global $post;

	// 	switch ($column) {
	// 		case "full_name":
	// 			$custom = get_post_custom();
	// 			echo "<a href='post.php?post=" . $post->ID . "&action=edit'>" . $custom["first_name"][0] . " " . $custom["last_name"][0] . "</a>";
	// 		break;
	// 	}
	// }

// LISTING FUNCTION
	function listGalleries($context, $idArray = null){
		global $post;
		global $galleriesMetaBoxArray;
		
		switch ($context) {
			case 'sort':
				$args = array(
					'post_type'  => 'galleries',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true
				);
				$loop = new WP_Query($args);

				echo '<ul class="sortable">';
				while ($loop->have_posts()) : $loop->the_post(); 
					$output = get_the_title($post->ID);//get_post_meta($post->ID, 'first_name', true) . " " . get_post_meta($post->ID, 'last_name', true);
					include(TEMPDIR . '/views/item_sortable.php');
				endwhile;
				echo '</ul>';
			break;
			
			case 'json':
				$args = array(
					'post_type'  => 'galleries',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true
				);
				returnData($args, $galleriesMetaBoxArray, 'json', 'galleries_data');
			break;

			case 'array':
				$args = array(
					'post_type'  => 'galleries',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true,
					'post__in' => $idArray
				);
				return returnData($args, $galleriesMetaBoxArray, 'array');
			break;

			case 'rest':
				$args = array(
					'post_type'  => 'galleries',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true,
					'post__in' => $idArray
				);
				return returnData($args, $galleriesMetaBoxArray, 'array');
			break;

			case 'inputs':
				$args = array(
					'post_type'  => 'galleries',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true
				);

				$outputArray = returnData($args, $galleriesMetaBoxArray, 'array');

				$field_options = array();
				foreach ($outputArray as $key => $value) {
					$checkBoxOption = array(
						"id" => $value['post_id'],
						"name" => html_entity_decode($value['the_title']),
					);
					$field_options[] = $checkBoxOption;
				}

				return $field_options;

			break;

		}
	}

?>
