"_modelSlug__sample_text_meta" => array(
	"id" => "_modelSlug__sample_text_meta",
    "name" => "Sample Text",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_text",
    	"input_name" => "sample_text"
    )
),
"_modelSlug__sample_date_meta" => array(
	"id" => "_modelSlug__sample_date_meta",
    "name" => "Sample Date",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_date",
    	"input_name" => "sample_date"
    )
),
"_modelSlug__sample_color_meta" => array(
	"id" => "_modelSlug__sample_color_meta",
    "name" => "Sample Color",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_colorpicker",
    	"input_name" => "sample_color",
    	"input_palette" => array(
    		'rgb(0, 59, 168);',
    		'rgb(102, 153, 51);',
			'rgb(53, 109, 211);',
			'rgb(95, 136, 211);',
    	)
    )
),
"_modelSlug__sample_editor_meta" => array(
	"id" => "_modelSlug__sample_editor_meta",
    "name" => "Sample Editor",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_editor",
    	"input_name" => "sample_editor"
    )
),
"_modelSlug__sample_checkbox_multi_meta" => array(
	"id" => "_modelSlug__sample_checkbox_multi_meta",
    "name" => "Sample Checkbox Multi",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_checkbox_multi",
    	"input_source" => "list_modelPluralSlug_",
    	"input_name" => "sample_checkbox_multi"
    )
),
"_modelSlug__sample_radio_meta" => array(
	"id" => "_modelSlug__sample_radio_meta",
    "name" => "Sample Radio",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_radio",
    	"input_source" => "list_modelPluralSlug_",
    	"input_name" => "sample_radio"
    )
),
"_modelSlug__sample_select_meta" => array(
	"id" => "_modelSlug__sample_select_meta",
    "name" => "Sample Select",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_select",
    	"input_source" => "list_modelPluralSlug_",
    	"input_name" => "sample_select"
    )
),
"_modelSlug__sample_checkbox_single_meta" => array(
	"id" => "_modelSlug__sample_checkbox_single_meta",
    "name" => "Sample Checkbox Single",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_checkbox_single",
    	"input_name" => "_modelSlug__sample_checkbox_single",
    	"input_text" => "Sample Option"
    )
),
"_modelSlug__sample_focuspoint_meta" => array(
	"id" => "_modelSlug__sample_focuspoint_meta",
    "name" => "Sample Focus Point",
    "post_type" => "_modelSlug_",
    "position" => "normal",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_focuspoint",
    	"input_name" => "_modelSlug__sample_focuspoint"
    )
),
"_modelSlug__sample_hidden_meta" => array(
	"id" => "_modelSlug__sample_hidden_meta",
    "name" => "Sample Hidden",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_hidden",
    	"input_name" => "_modelSlug__sample_hidden"
    )
),
"_modelSlug__featured_images_meta" => array(
	"id" => "_modelSlug__featured_images_meta",
    "name" => "Featured Images",
    "post_type" => "_modelSlug_",
    "position" => "side",
    "priority" => "low",
    "callback_args" => array(
    	"input_type" => "input_featured_images",
    	"input_name" => "_modelSlug__featured_images"
    )
),