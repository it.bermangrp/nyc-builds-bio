(function($){

	boilerplate = function(variableName){
		this.ajaxer = (typeof ajaxer === "undefined") ? false : ajaxer;
		if($('html').hasClass('internet-explorer-8')){
			this.ie8support();
		}
	};

	boilerplate.prototype.multiLevelPushMenu = function(target, position) {

		position = (typeof position === "undefined") ? 'right' : position;

		targetClone = target;
		targetClone.find('*').removeAttr('class')
		targetClone.removeAttr('class')

		targetClone.mmenu({
			autoHeight: true,
			offCanvas: {
				position: position
			},
			onClick: {
				close: function() {
					return ($(this).attr('data-pageid') != "");
				}
			}
		}, {
			clone: true
		});

		$('.mm-page').append($('.pageSection'))
		$('.mm-page').append($('.footer'))

		$('.navbar-toggle').on('click', function(e){
			if($('#mm-navbar').hasClass('mm-opened')){
				$("#mm-navbar").trigger("close.mm");
			} else {
				$("#mm-navbar").trigger("open.mm");
			}
		});

	}

	boilerplate.prototype.get = {
		images_data: function(array_of_ids, callback){
			var that = this;
			var these_images = [];
			var these_images_info = [];
			if(array_of_ids.length > 0 && array_of_ids[0] != ''){
				$.post(that.pageDir + '/machines/handlers/loadPost.php', {'postRequest': array_of_ids}, function(images_data){
					_.each(array_of_ids, function(value_array_of_ids, index_array_of_ids){
						these_images_info.push(images_data[value_array_of_ids]);
						these_images.push(images_data[value_array_of_ids].image_data.full);
					})
					callback(these_images, these_images_info);
				}, "json")
			} else {
				callback(these_images, these_images_info);
			}
		},
		data: function(array_of_models, done_function){
			var that = this;
			var data_fetched = array_of_models.length;
			var data = {};
			$.each(array_of_models, function(index_array_of_models, value_array_of_models){
				that.returnAndSaveJsonData(value_array_of_models, function(modelData){
					data_fetched--;
					data[value_array_of_models] = modelData;
					if(data_fetched == 0){
						done_function(data);
					}
				});
			});
		}
	};

	boilerplate.prototype.loadView = function(pageIDrequest, postIDrequest){
		var that = this;
		that.cleanUp(pageIDrequest, postIDrequest);

		if( that.isSinglePage() ){
			that.returnAndSaveJsonData('listPages', function(pagesData){
				var pageListing = [that.defaultPage];
				$('#navbar').find('#menu-main-menu').find('a').each(function(){
					if($.inArray($(this).attr('data-pageid'), pageListing) == -1){
						pageListing.push($(this).attr('data-pageid'))
					}
				})


				_.each(pageListing, function(value_pageListing, index_pageListing){
					var viewName = (typeof that.viewRender[value_pageListing] === 'undefined') ? 'default' : value_pageListing;
					that.viewRender[viewName](that, value_pageListing);
				})

				setTimeout(function(){
					that.singlePageAnimate(pageIDrequest)
				}, 400)
			})
		} else {

			pageAttrID = ($.isNumeric(pageID.charAt(0))) ? "_" + pageID : pageID;
			$('body').data('pageid', pageID);
			$('body').attr('id', pageAttrID);

			var viewName = (typeof that.viewRender[pageIDrequest] === 'undefined') ? 'default' : pageIDrequest;
			that.viewRender[viewName](that);

		}

	}


	boilerplate.prototype.executeLogin = function(redirectPage){
		var that = this;
		Shadowbox.init();
		Shadowbox.open({
			// player: 'iframe',
			// content: 'http://www.ifmacap.org/preview/wp-content/themes/ccifma/machines/handlers/linkedin.api.php'
			content: '<div class="popUpLoginForm" />',
			player: 'html',
			options: {
				onFinish: function() {
					formObject = $(that.php_ajax_login);
					$('.popUpLoginForm').html(formObject)

					thisForm = $('.popUpLoginForm form');

					// handle submit
					thisForm.on('submit', function(e) {
						e.preventDefault();

						loginInfoArr = {
							'username': $(this).find('#user_login').val(),
							'password': $(this).find('#user_pass').val()
						}

						$.post(that.pageDir + "/machines/handlers/ajaxLogin.php", loginInfoArr, function(data) {
							if (data == 'success') {
								Shadowbox.close();
								js_user_logged_in = true;
								window.location.href = that.pathPrefix + redirectPage;
							} else {
								if (thisForm.find('.wrongLogin').size() <= 0 ) {
									thisForm.append('<p class="wrongLogin">Wrong username or password</p>');
								}
							}
						});

					});
				}
			}
		});
	}

	boilerplate.prototype.externalURL = function(urlRequest){
		return (urlRequest.indexOf("http:") == -1 && urlRequest.indexOf("https:") == -1) ? "http://" + urlRequest : urlRequest;
	}

	boilerplate.prototype.renderModel = function(data, viewObject){

		var that = this;

		_.each(data, function(value, index){
			switch (index){

				case 'the_content':
					viewObject.find('.' + index).html(_.unescape(value));
					that.handle_internal_links({
						target_element: viewObject.find('.' + index)
					})
				break;

				default:
					viewObject.find('.' + index).html(_.unescape(value));
				break;
			}
		});

		return viewObject;
		
	}

	boilerplate.prototype.returnAndSaveJsonData = function(jsonRequest, callback){
		var that = this;
    	if(typeof that.jsonDataCollection === "undefined"){
    		that.jsonDataCollection = {};
    	}
        args = {};
        args['idArray'] = null;
        if(typeof callback !== "undefined"){
	        if(typeof that.jsonDataCollection[jsonRequest] === "undefined"){
		        returnedJsonData = $.post(that.pageDir + "/machines/handlers/loadJSON.php", { jsonRequest: jsonRequest, args: args }, function(data) {
		        	that.jsonDataCollection[jsonRequest] = data;
					that.createAccessor(data, jsonRequest);
		        	callback(data);
		        }, 'json');
		    } else {
				that.createAccessor(that.jsonDataCollection[jsonRequest], jsonRequest);
		    	callback(that.jsonDataCollection[jsonRequest])
		    }
        } else {
	        if(typeof that.jsonDataCollection[jsonRequest] === "undefined"){
		        returnedJsonData = $.post(that.pageDir + "/machines/handlers/loadJSON.php", { jsonRequest: jsonRequest, args: args }, function(data) {
		        	that.jsonDataCollection[jsonRequest] = data;
					that.createAccessor(data, jsonRequest);
		        	return data;
		        }, 'json');
		    } else {
				that.createAccessor(that.jsonDataCollection[jsonRequest], jsonRequest);
		    	return that.jsonDataCollection[jsonRequest];
		    }
        }
    }

	boilerplate.prototype.createAccessor = function(dataSource, variableName){
		var that = this;
		if(typeof that['accessor_' + variableName] === "undefined"){
			that['accessor_' + variableName] = {};
			that['accessor_ids_' + variableName] = {};
			that['accessor_slug_' + variableName] = {};
			if(variableName == 'listPages'){
				_.each(dataSource, function(valuePagesData, indexPagesData){
					that['accessor_' + variableName][that.slugify(valuePagesData.the_title)] = valuePagesData;
					that['accessor_ids_' + variableName][valuePagesData.post_id] = valuePagesData;
				});
			} else {
				_.each(dataSource, function(valuePagesData, indexPagesData){
					that['accessor_' + variableName][valuePagesData.post_id] = valuePagesData;
					that['accessor_slug_' + variableName][that.slugify(_.unescape(valuePagesData.the_title))] = valuePagesData;
				});
			}
		}
	}

    boilerplate.prototype.startUp = function(){
    	var that = this;
    	that.setPageID(that.RewriteBase);

		if(window['pageID'] == "admin"){
			that.loadDatePicker($('.date'));
			that.loadSortable($(".sortable"));
			if($('#people_sample_hidden_meta').size() != 0){
				$('#people_sample_hidden_meta').find('.hidden_meta').val('I was generated dynamically')
			}
			$('head').append('<link rel="stylesheet" id="jquery-style-css" href="' + that.pageDir + '/styles/admin-styles.min.css" type="text/css" media="all">');
			that.adminPages(pagenow);
		} else {
			that.setPopstate();
			that.fixLinks();
			that.loadJqueryHelpers({ 
				'bsSlider': that.bsSlider 
			});

			if(typeof pageID !== "undefined"){
				that.loadView(pageID, postID);
				that.startUpRan = true;
			}
		}
    };

	boilerplate.prototype.fixLinks = function(params){
		var that = this;
		var redirect_pages = (typeof that.redirect_pages === 'undefined') ? [] : that.redirect_pages;
		$('.navbar, .footer').find('a').each(function(){
			var isInternalLink = ($(this).hasClass('hyperlink')) ? false : true;
			var isNotEmail =  ($(this).attr('href').indexOf('mailto:') == -1) ? true : false;
			var isLogo = ($(this).hasClass('navbar-brand')) ? true : false;
			var isParent = ($(this).siblings('.sub-menu').length > 0) ? true : false;


			if(isInternalLink && isNotEmail){
				var pageSlug = (isLogo) ? that.defaultPage : that.slugify($(this).text().replace('&amp;', ''))
				pageSlug = (isParent && (redirect_pages.indexOf(that.slugify($(this).text().replace('&amp;', ''))) > -1)) ? that.slugify($(this).siblings('.sub-menu').children('li').eq(0).text().replace('&amp;', '')) : pageSlug;
				var newURL = '/' + pageSlug + '/';
				$(this).attr('href', newURL);
				$(this).addClass('menu-item-' + pageSlug);
				$(this).attr('data-pageid', pageSlug);

				$(this).off('click');
				$(this).on('click', function(e){
					e.preventDefault();
					that.pushPageNav($(this).attr('data-pageid'))
				})
			}
		});
	}

    boilerplate.prototype.animateSomething = function(params){
    	$(params.target).animate(params.animation, params.time, params.callback)
    }

    boilerplate.prototype.isSinglePage = function(){
    	var that = this;
    	return (typeof this.singlePage !== "undefined" && this.singlePage != null && this.singlePage != false);
    }

    boilerplate.prototype.check_animation_skip = function(previousPage, currentPage){
    	var that = this;
    	var doAnimation = true;
    	if(typeof that.skip_animation_pages !== 'undefined'){
			$.each(that.skip_animation_pages, function(index_skip_animation_pages, value_skip_animation_pages){
				if(value_skip_animation_pages == currentPage && value_skip_animation_pages == previousPage){
	    			doAnimation = (previousPage == value_skip_animation_pages && currentPage == value_skip_animation_pages) ? false : doAnimation;
				}
			});
    	}
		return doAnimation;
	}


	boilerplate.prototype.pushPageNav = function(pageIDrequest, postIDrequest){

		var that = this;
		var postIDrequest = (typeof postIDrequest === 'undefined') ? null : postIDrequest;
		var postIDurl = "";
		var newPage;

    	if(postIDrequest == null){

    		postIDrequest = "";
    		postIDurl = "";

    	} else {

    		postIDurl = postIDrequest + "/";

    	}

		if( that.isSinglePage() ){

			if((window.location.pathname.charAt(window.location.pathname.length-1) == "/") && (window.location.pathname != that.RewriteBase)){
				newPage = '/' + pageIDrequest + "/" + postIDurl;
			} else {
				newPage = '/' + pageIDrequest + "/" + postIDurl;
			}

			var stateObj = { pageID: newPage };

	        if (Modernizr.history){
				history.pushState(stateObj, null, newPage);
	        }

			that.setPageID(that.RewriteBase);

			that.singlePageAnimate(pageIDrequest);

		} else {

	        if (Modernizr.history){

				var previousPage = pageID + "";
	        	newPage = '/' + pageIDrequest + "/" + postIDurl;

	    		var stateObj = { pageID: newPage };

	    		history.pushState(stateObj, null, newPage);

	    		that.setPageID(that.RewriteBase)
				that.gaSendPageView();

	        	if(that.check_animation_skip(previousPage, pageID)){

					that.animateSomething({
						target: $('.pageSection'),
						animation: {
							opacity: 0,
						},
						time: 200,
						callback: function() {

							that.loadView(pageID, postID);

						}
					});

	        	} else {

	        		that.loadView(pageID, postID);

	        	}

	        } else {

				window.location = that.pathPrefix + pageIDrequest + "/" + postIDurl;

	        }

		}

    }

    boilerplate.prototype.setPopstate = function(){
    	var that = this;
		if( that.isSinglePage() ){

		    $(window).on('popstate',function(){
				if(that.startUpRan){
					postID = "";
					previousPage = pageID;


			    	if(history.state != null){
						that.setPageID(that.RewriteBase, history.state.pageID);
						that.singlePageAnimate(pageID)

			    	} else {
		    			loadPage = that.cleanPageState(window.location.pathname.replace(that.RewriteBase, ""));

		    			postIDtest = window.location.pathname.split("/");
		    			if(postIDtest[postIDtest.length-1] == ""){
		    				postIDtest.pop();
		    			}
		    			if($.isNumeric(postIDtest[postIDtest.length-1])){
		    				postID = postIDtest[postIDtest.length-1];
		    			}

		    			if(loadPage == ""){
		    				loadPage = that.defaultPage;
		    			}
		    			pageID = loadPage;

		    			postIDpath = (history.state == null) ? "" : history.state.pageID;
						that.setPageID(that.RewriteBase, postIDpath);
						that.singlePageAnimate(pageID)

			    	}
			    }
		    });


		} else {

		    $(window).on('popstate',function(){

				if(that.startUpRan){

					postID = "";
					previousPage = pageID + "";

			    	if(history.state != null){

						that.setPageID(that.RewriteBase, history.state.pageID);
						that.gaSendPageView();

			    		if(that.check_animation_skip(previousPage, pageID)){

							that.animateSomething({
								target: $('.pageSection'),
								animation: {
									opacity: 0,
								},
								time: 200,
								callback: function(){

					        		that.loadView(pageID, postID);

								}
							})

			    		} else {

			    			that.loadView(pageID, postID);

			    		}
			    	} else {

		    			var loadPage = that.cleanPageState(window.location.pathname.replace(that.RewriteBase, ""));

		    			var postIDtest = window.location.pathname.split("/");

		    			if(postIDtest[postIDtest.length-1] == ""){

		    				postIDtest.pop();

		    			}

		    			if($.isNumeric(postIDtest[postIDtest.length-1])){

		    				postID = postIDtest[postIDtest.length-1];

		    			}

		    			if(loadPage == ""){

		    				loadPage = that.defaultPage;

		    			}

		    			pageID = loadPage;

		    			var pastState = (history.state != null) ? history.state.pageID : "";
		    			that.setPageID(that.RewriteBase, pastState);
						that.gaSendPageView();

						if(that.check_animation_skip(previousPage, pageID)){

							that.animateSomething({
								target: $('.pageSection'),
								animation: {
									opacity: 0,
								},
								time: 200,
								callback: function(){
									that.loadView(pageID, postID);
								}
							})

						} else {

							that.loadView(pageID, postID);

						}

			    	}
			    }

		    });

		}

    }

	boilerplate.prototype.changePage = function(transition){
		var that = this;
		that.animateSomething({
			target: $('.pageSection'),
			animation: {
				opacity: 1,
			},
			time: 200,
			callback: function(){
				that.animateSomething({
					target: $('html,body'),
					animation: {
						scrollTop: 0 
					},
					time: 100,
					callback: function(){
					}
				})
			}
		})
	}

	boilerplate.prototype.cleanPageState = function(historyState){
		postIDtest = historyState.split("/");
		if(postIDtest[postIDtest.length-1] == ""){
			postIDtest.pop();
		}
		if($.isNumeric(postIDtest[postIDtest.length-1])){
			postIDtest.pop();
			historyState = postIDtest.join("/");
		} else {
			historyState = postIDtest[0];
		}
		if(typeof historyState !== "undefined"){
			historyState = historyState.replace("../","");
			historyState = historyState.replace("/","");
			historyState = historyState.replace(/\.\.+/g, '');
			historyState = historyState.replace(/\/+/g, '');
		} else {
			historyState = "";
		}
		return historyState;
	}

	boilerplate.prototype.slugify = function(text){

		slugText = text.replace(/&amp;/g, '')
		    .replace(/#8216;/g, '')
		    .replace(/#038;/g, '')
		    .replace(/#8217;/g, '');


		slugText = slugText.toString().toLowerCase()
			.replace(/\+/g, '')           // Replace spaces with 
			.replace(/\s+/g, '-')           // Replace spaces with -
			.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
			.replace(/\-\-+/g, '-')         // Replace multiple - with single -
			.replace(/^-+/, '')             // Trim - from start of text
			.replace(/-+$/, '');            // Trim - from end of text

		return slugText;

	}

	boilerplate.prototype.getUrlVars = function(params){
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });
	    return vars;
	};

	boilerplate.prototype.start = function(params){
		this.configDevMode();
		this.configParams(params);

		this.startUpRan = false;
		this.pathPrefix = window.location.protocol + "//" + window.location.host + "/";
		this.defaultPageDir = window.location.protocol + "//" + window.location.host + "/wp-content/themes/" + this.themeFolderName;
		this.pageDir = (typeof $('body').attr('data-tempdir') === "undefined") ? this.defaultPageDir : $('body').attr('data-tempdir');
		this.urlQueryString = "?" + Math.floor((Math.random()*10000)+1);

		if(typeof this.typekitID !== "undefined" && this.typekitID != null){
			this.loadTypekit(this.typekitID);
		} else {
			this.loadFilesToVariables();
		}
	};

	boilerplate.prototype.loadFilesToVariables = function() {
		var that = this;
		var fileArray = that.filesToVariablesArray;

        var ajaxRequests = [];
        _.each(fileArray, function(value_fileArray, index_fileArray){
        	var keyName = Object.keys(fileArray[index_fileArray]);
        	var variableURL = fileArray[index_fileArray][keyName];
        	var fileType = variableURL.split(".").slice(-1)[0];
        	var variableName = fileType + "_" + keyName;

	        ajaxRequest = $.ajax({
	            type: 'GET',
	            dataType: "text",
	            url: that.defaultPageDir + '/' + variableURL,
	            variableName: variableName,
	            variableURL: variableURL,
	            cache: false,
	            success: function (data, textStatus, jqXHR) {
	            	that[this.variableName] = data;
	            },
	            error: function (argument) {
	            }
	        });
	        ajaxRequests.push(ajaxRequest);
        });

        $.when.apply($, ajaxRequests).then(function() {
        	that.startUp()
        }, function() {
        	// errors occured
        });
    };

	boilerplate.prototype.loadSortable = function(target){
		target.sortable();
		target.disableSelection();
		target.on( "sortstop", function( event, ui ) {
			sortData = {};
			$(this).children('li').each(function(){
				sortData[$(this).data('id')] = $(this).index();
			});
			var data = {
				action: 'update_sort',
				sort_data: sortData
			};
			$.post(ajaxurl, data, function(response) {
			});					
		});
	}

	boilerplate.prototype.loadDatePicker = function(target, changeCallback){
		hiddenDate = target.siblings('input');
		target.datetimepicker();
		if(hiddenDate.val() == ""){
			var myDate = new Date();
			var prettyDate =(myDate.getMonth()+1) + '/' + myDate.getDate() + '/' + myDate.getFullYear() + " " + myDate.getHours() + ":" + myDate.getMinutes();
			target.val(prettyDate);
			hiddenDate.val(Date.parse(prettyDate)/1000);
		}
		target.change(function() {
			$(this).siblings('input').val(Date.parse($(this).val())/1000);
			dateArray = [{event_start: $('input[name="event_start"]').val(), event_end: $('input[name="event_end"]').val()}];
			$('#event_date_array_meta').find('.hidden_meta').val(JSON.stringify(dateArray));
			if(changeCallback){
				updateRepeatConfig();
			}
		});
	}

	boilerplate.prototype.renderGalleriesAdmin = function(){
		var that = this;
		$.post( that.pageDir + "/machines/handlers/getGalleries.php", {  }, function( data ) {
			// get all images from database and save to global variable
			window['galleryImagesObject'] = data;

			// append images to all images box
			_.each(data, function(value, index){
				returnObject = buildThumbnails(value, index);
				$('#galleries_all_images_meta').find('.inside').append(returnObject);
			});

			// css cleanup for side images box
			$('#galleries_all_images_meta').find('img').on('load', function(){
				$('#galleries_gallery_images_meta').css('min-height', $( "#galleries_all_images_meta" ).height())
				$('#galleries_gallery_images_meta').find('.inside').css('min-height', Math.round($( "#galleries_all_images_meta" ).height()))
			});

			// load jQuery UI draggable
			$('#galleries_all_images_meta').find('.inside').children().draggable({
				revert: true
			});

			$( "#galleries_gallery_images_meta" ).find('.inside').droppable({
				hoverClass: "greyBackground",
				drop: function( event, ui ) {
					// load jQuery UI draggable
					if(ui.helper.parents('.postbox').attr('id') == "galleries_all_images_meta"){
						thisObject = $(ui.helper.html());
						returnObject = buildThumbnails(galleryImagesObject[thisObject.attr('data-postid')], thisObject.attr('data-postid'), true);
						$( "#galleries_gallery_images_meta" ).find('.inside').append(returnObject);
						$( "#galleries_gallery_images_meta" ).find('.inside').addClass('added')
						$( "#galleries_gallery_images_meta" ).find('.inside').addClass('transitionClass')
						saveGallery();
						updateGalleryEvents();
					}
				}
			});

			if($('#galleries_gallery_image_array').val() != ""){
				// check if this is a saved post and update the sidebar accordingly
				currentImageArray = $('#galleries_gallery_image_array').val().split(",");
				_.each(currentImageArray, function(value, index) {
					_.each(galleryImagesObject, function(value1, index1){
						if(value == index1){
							returnObject = buildThumbnails(value1, index1, true);
							$( "#galleries_gallery_images_meta" ).find('.inside').append(returnObject);
						}
					})
				})
				updateGalleryEvents();
			}
		}, "json");

		function buildThumbnails(imageData, imageID, sidebarThumb){
			returnObject = $('<div class="imageContainer" />')
			imageBlock = $('<div class="imageBlock" data-postid="' + imageID + '" />');
			imageThumb = $('<img />');
			imageThumb.attr('src', imageData.thumb);
			imageBlock.append(imageThumb);
			returnObject.append(imageBlock)
			if(sidebarThumb){
				boxControls = $('<div class="boxControls" />');
				deleteButton = $('<div class="deleteButton">remove</div>')
				boxControls.append(deleteButton)
				returnObject.append(boxControls)
				returnObject.append(imageBlock)
			}
			return returnObject;
		}

		function saveGallery(){
			outputArray = [];
			$('#galleries_gallery_images_meta').find('.imageBlock').each(function(){
				outputArray.push($(this).attr('data-postid'))
			})
			$('#galleries_gallery_image_array').val(outputArray)
		}

		function updateGalleryEvents(){
			setTimeout(function(){
				$( "#galleries_gallery_images_meta" ).find('.inside').removeClass('added')
			}, 400);

			setTimeout(function(){
				$( "#galleries_gallery_images_meta" ).find('.inside').removeClass('transitionClass')
			}, 500);

			$('#galleries_gallery_images_meta').find('.ui-droppable').sortable({
			});

			$('#galleries_gallery_images_meta').off( "sortstop")
			$('#galleries_gallery_images_meta').on( "sortstop", function( event, ui ) {
				saveGallery();
			});

			$('.deleteButton').off('click')
			$('.deleteButton').on('click', function(){
				$(this).parents('.imageContainer').remove()
				saveGallery();
			})
		}
	}

	boilerplate.prototype.featuredImagesLauncher = function(){
		var that = this;
		Shadowbox.init();
		$('.featured_images_launcher').on('click', function(){
			Shadowbox.open({
				content:    '<div class="featuredImages"><div class="frame"></div><div class="frame"></div></div>',
				player:     "html",
				title:      "Welcome",
				height:     100000000,
				width:      100000000,
				options: {onFinish: function () {
					frame1 = $('.featuredImages').find('.frame').eq(0);
					frame2 = $('.featuredImages').find('.frame').eq(1);

					frame1.addClass('imagePicker')
					frame2.append('<div class="imageOrderer" />')

					$.post( that.pageDir + "/machines/handlers/getGalleries.php", {  }, function( data ) {
						// get all images from database and save to global variable
						window['galleryImagesObject'] = data;

						_.each(data, function(image_value, image_index){
							returnObject = $('<div class="imageThumb" />');
							imageObject = $('<img />');
							imageObject.attr('src', image_value.thumb)
							returnObject.append(imageObject)
							returnObject.attr('data-index', image_index)
							returnObject.data('imageIndex', '.imageIndex' + image_index)
							returnObject.addClass('imageIndex' + image_index)
							frame1.append(returnObject)
						});

						$('.imageOrderer').sortable();
						$('.imageOrderer').on( "sortstop", function( event, ui ) {
							outputArray = [];
							$('.imageOrderer').find('.imageThumb').each(function(){
								outputArray.push($(this).attr('data-index'))
							})
							$('.featured_images_meta').val(outputArray)
						});

						if($('.featured_images_meta').val() != ""){
							selectedImages = $('.featured_images_meta').val().split(",")
							$.each(selectedImages, function(index, value){
								$('.imagePicker').find('.imageIndex' + value).addClass('selected')
								frame2.find('.imageOrderer').append($('.imagePicker').find('.imageIndex' + value).clone())
							});
						}

						frame1.find('.imageThumb').on('click', function(){
							if($(this).hasClass('selected')){
								$(this).removeClass('selected')
								targetClass = $(this).data('imageIndex')
								frame2.find(targetClass).remove()
							} else {
								$(this).addClass('selected')
								duplicateImage = $(this).clone()
								duplicateImage.removeClass('selected')
								frame2.find('.imageOrderer').append(duplicateImage)
							}
							outputArray = [];
							$('.imageOrderer').find('.imageThumb').each(function(){
								outputArray.push($(this).attr('data-index'))
							})
							$('.featured_images_meta').val(outputArray)
						});

					}, "json");								

				}} 

			});
		})
	}

	boilerplate.prototype.setPageID = function(pagePath, postIDpath){
		var that = this;
		if($('body').hasClass("wp-admin")){
			window['pageID'] = "admin";
		} else {
			var pageIDFound = false;
			var urlArray = window.location.pathname.replace(pagePath, '');
				urlArray = urlArray.split("/");

			if(urlArray[urlArray.length-1] == ""){
				urlArray.pop();
			}

			if(typeof urlArray[0] === "undefined"){
				pageIDFound = true;
				window['pageID'] = this.defaultPage;
			} else {
				_.each(json_pages, function(value, index){
					if(value.pageID == urlArray[0]){
						pageIDFound = true;
						window['pageID'] = value.pageID;
					}
				});
			}

			if(!pageIDFound){
				if(!that.ajaxer){
					that.execute404();
				}
			} else {
			// second array item must be itemID
				if(urlArray.length == 2){
					switch(urlArray[0]){
						default:
							postIDFound = true;
							window['postID'] = urlArray[1];
						break;
					}
				} else {
					window['postID'] = "";
				}

			}
		}
	};

	boilerplate.prototype.execute404 = function(){
		$('section').html("<h1>This page was not found, you are being redirect to the home page</h1>");
		window.location = this.pathPrefix + this.defaultPage;
	}

	boilerplate.prototype.configParams = function(paramsRequest){
		var that = this;
		_.each(paramsRequest, function(value_params, index_params){
			switch(index_params){
				case 'themeFolderName':
					var urlVars = that.getUrlVars();
					if(typeof urlVars.dev !== "undefined" && urlVars.dev != "" ){
						themeAppendString = urlVars.dev;
					} else {
						themeAppendString = "dev";
					}
					that[index_params] = (typeof js_dev_mode === "undefined" || js_dev_mode == "false") ? value_params : value_params + "-" + themeAppendString;
				break;

				default:
					that[index_params] = value_params;
				break;
			}
		});

		_.each(that.helpers, function(value_helper, index_helper){
			boilerplate.prototype[index_helper] = value_helper;
		});

	};

	boilerplate.prototype.configDevMode = function(){
		if(typeof js_dev_mode !== "undefined" && js_dev_mode == "true" && Modernizr.history && (js_dev_refresh == "true")){
			history.pushState(null, null, window.location.origin + window.location.pathname);
		}
	};

	boilerplate.prototype.loadTypekit = function(typekitID){
		var that = this;
		var config = {
			kitId: typekitID,
			scriptTimeout: 1000,
			loading: function() {
				// JavaScript to execute when fonts start loading
			},
			active: function() {
				that.loadFilesToVariables();
			},
			inactive: function() {
				that.loadFilesToVariables();
			}
		};
		var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
	};

	boilerplate.prototype.saveAjax = function() {
		var that = this;
		var newDOM = $('html').clone();

		$.post( that.pageDir + "/machines/handlers/saveAjax.php", { pageID: pageID, postID: postID, html: newDOM[0].outerHTML })
		.done(function(data) {
			console.log(data)
		});
	}

	// pageID should be undefiend for listPages
	// loop will take 5 seconds between pages
	// 2 second delay to execute saveAhax
	boilerplate.prototype.saveAjaxArray = function(modelList, pageID) {
		var that = this;
		that.returnAndSaveJsonData(modelList, function(modelData){
			var index = 0;
			window['switchLoop'] = setInterval(function(){
				if(typeof pageID == 'undefined'){
					var postID = that.slugify(modelData[index].the_title);
					that.pushPageNav(postID);
				} else {
					var postID = that.slugify(modelData[index].the_title) + '-' + modelData[index].post_id;
					that.pushPageNav(pageID,  postID);
				}

				setTimeout(function(){
					that.saveAjax()
					console.log(index, modelData.length-1);

					if( index == modelData.length-1 ) {
						clearInterval(switchLoop)
					} else {
						index++
					}
				}, 2000)
			}, 5000);
		});
	};

	/**
	 * [xmlSitemap generate site]
	 * @param  {array} models array of objects
	 * 
	 * exp: [{
	 * 	
	 * 	LIST MODEL 
	 * 	modelName:'listProjects', 
	 * 	
	 * 	MODEL PAGE ID LEAVE BLANK IF NONE
	 * 	modelUrl: '/portfolio/', 
	 * 	
	 * 	ARRAY FORMING SINGLE PAGE URL NAME
	 * 	THE SAMPEL BELOW WILL GENERATE
	 * 	sample_page-1/
	 * 	pageName: ['{the_title}', '-' '{post_id}']
	 * 	}]
	 */
	
	 /* 
	 EXAMPLE:
	 webApp.xmlSitemap([{ 
	 	modelName:'listPages', 
	 	modelUrl: '/page',
	 	pageName: ['the_title', '-', 'post_id']
	 }, { 
	 	modelName:'listPeople', 
	 	modelUrl: '/news',
	 	pageName: ['the_title']
	 }])
*/
	boilerplate.prototype.saveXMLSitemap = function(models) {
		var that = this;
		var sitemapArray = [
			'<?xml version="1.0" encoding="UTF-8"?>',
			'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'
		]

		buildXML = function(index){
			var modelData = models[index];
			var nextIndex = (index+1);

			var listModel = modelData.modelName;
			var modelPath = location.origin;
			var pageName = modelData.pageName

			modelData.modelUrl = ( _.isUndefined(modelData.modelUrl) )? '/' : modelData.modelUrl;
			// check modeUrl starts with /
			modelData.modelUrl = ( modelData.modelUrl.charAt(0) == '/' )? modelData.modelUrl : '/' + modelData.modelUrl;
			// check modeUrl ends with /
			modelData.modelUrl = ( modelData.modelUrl.charAt(modelData.modelUrl.length-1) == '/' )? modelData.modelUrl : modelData.modelUrl + '/';
			
			modelPath += modelData.modelUrl;

			that.returnAndSaveJsonData(listModel, function(singleModelData){
				_.each(singleModelData, function(singleItemValue, singleItemIndex){
					var pageSlug = '';
					
					_.each(pageName, function(nameParam, nameIndex){
						if( _.isUndefined(singleItemValue[nameParam]) ){
							pageSlug += nameParam
						} else {
							pageSlug += that.slugify( String(singleItemValue[nameParam]) );
						}
					});

					var pageObj = $('<url/>');
					var locObj = $('<loc/>', {
						html: modelPath + pageSlug + '/'
					});
					var freqObj = $('<changefreq/>', {
						html: 'daily'
					});

					pageObj.append('\n', locObj, '\n', freqObj, '\n');
					sitemapArray.push(pageObj[0].outerHTML)
				});

				console.log(models.length, nextIndex, models.length == nextIndex)
				if( models.length == nextIndex ){
					saveData();
				} else {
					buildXML(nextIndex);
				}
			});
		}

		saveData = function(){
			sitemapArray.push('</urlset>');
			
			$.post(that.pageDir + "/machines/handlers/saveSitemap.php", {
				data: sitemapArray.join('\n')
			}, function(response){
				console.log(response)
			});
		}

		buildXML(0);
	};

	boilerplate.prototype.ie8support = function(){
		// object.keys support
		    if (!Object.keys) {
		        Object.keys = (function () {
		            'use strict';
		            var hasOwnProperty = Object.prototype.hasOwnProperty,
		            hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
		            dontEnums = [
		            'toString',
		            'toLocaleString',
		            'valueOf',
		            'hasOwnProperty',
		            'isPrototypeOf',
		            'propertyIsEnumerable',
		            'constructor'
		            ],
		            dontEnumsLength = dontEnums.length;
		            return function (obj) {
		                if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
		                    throw new TypeError('Object.keys called on non-object');
		                }
		                var result = [], prop, i;
		                for (prop in obj) {
		                    if (hasOwnProperty.call(obj, prop)) {
		                        result.push(prop);
		                    }
		                }
		                if (hasDontEnumBug) {
		                    for (i = 0; i < dontEnumsLength; i++) {
		                        if (hasOwnProperty.call(obj, dontEnums[i])) {
		                            result.push(dontEnums[i]);
		                        }
		                    }
		                }
		                return result;
		            };
		        }());
		    }

		// console support
	    var method;
	    var noop = function () {};
	    var methods = [
	        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
	        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
	        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
	        'timeStamp', 'trace', 'warn'
	    ];
	    var length = methods.length;
	    var console = (window.console = window.console || {});
	    while (length--) {
	        method = methods[length];
	        if (!console[method]) {
	            console[method] = noop;
	        }
	    }
	};

    boilerplate.prototype.gaSendPageView = function () {			    				    	
    	var that = this, gaAnalyticsID = that.gaAnalyticsID;

    	if ( gaAnalyticsID && ( typeof gtag !== 'undefined' )) {
    		var url_string = ( that.postIDnotSet() ) ? '/' + pageID + '/' : '/' + pageID + '/' + postID + '/';
	    	gtag('config', gaAnalyticsID, { 'page_path': url_string });	
    	} else {
    		// console.warn('gaAnalyticsID is currently undefined. Define as property in webApp before invocation.');
    	}			    	
    }

	boilerplate.prototype.handle_internal_links = function(params){

		/* USAGE */
		// that.handle_internal_links({
		// 	target_element: slide_object
		// });

		var that = this;
		var target_element = (typeof params.target_element === 'undefined') ? $('body') : params.target_element;

		var internalize_link = function(params){
			var link_object = params.link_object;
			var page_data = params.page_data;
			link_object.addClass('internalLink');
			link_object.off('click');
			link_object.on('click', function(e){
				e.preventDefault();
				that.pushPageNav(that.slugify(_.unescape(page_data.the_title)))
			})
		}

		var scan_elements = function(){
			target_element.find('a').each(function(){
				var this_href = $(this).attr('href');
				var delimiter = '?page_id=';
				var delimiter_char_pos = this_href.indexOf(delimiter);
				if(delimiter_char_pos > -1){
					var wp_page_id = parseInt(this_href.slice(delimiter_char_pos + delimiter.length));
					var this_page_data = that.accessor_ids_listPages[wp_page_id];
					$(this).attr('href', '/' + that.slugify(_.unescape(this_page_data.the_title)) + '/')
					internalize_link({
						link_object: $(this),
						page_data: this_page_data
					});
				}
			})
		}

		scan_elements();

	}

	boilerplate.prototype.simpleSearch = function(searchTermArray, dataSource, inclusionArray) {
		
		inclusionArray = (typeof inclusionArray === "undefined") ? "" : inclusionArray;
		exclusionArray = ['attachments', 'post_id', 'featuredImage', 'partner_industry', 'partner_level'];
		resultArray = [];
		deleteArray = [];
		_.each(searchTermArray, function(value, index){
			searchTermArray[index] = value.replace(/\W/g, '');			
			if(searchTermArray[index] == ""){
				deleteArray.push(index);
			}
		});
		deleteArray = deleteArray.reverse();
		_.each(deleteArray, function(value, index){
			searchTermArray.splice(value, 1);
		})
		_.each(searchTermArray, function(value0, index0){
			_.each(Object.keys(dataSource[0]), function(value, index){

				if((inclusionArray == "") || (inclusionArray.indexOf(value) != -1)){

					if(exclusionArray.indexOf(value) == -1){
						results = _.filter(dataSource, function(results, second, third){
							if(typeof results[value] === "string"){
								return (results[value].toLowerCase().indexOf(value0) >= 0);
							}
						});
						resultArray.push(results);
					}
				}
			});
		});
		resultArray = _.union(resultArray);
		resultArray = _.flatten(resultArray);
		resultArray = _.union(resultArray);
		return resultArray;

	}

	boilerplate.prototype.dollarize = function(dollars){
		dollars = parseFloat(dollars).toFixed(0);
		dollars = "$" + String(dollars).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
		return dollars;
	}

	boilerplate.prototype.postIDnotSet = function(){
		return (typeof postID === "undefined") || (postID == "")
	}

	boilerplate.prototype.loadJqueryHelpers = function(functions) {
		// USAGE
		// that.loadJqueryHelpers({ 'bsSlider': that.bsSlider });

		for ( var funcName in functions ) { $.fn[funcName] = functions[funcName]; }
	}

	boilerplate.prototype.renderSlideshow = function(params){

			// // USAGE
			// 	that.renderSlideshow({
			// 		target_element: $('body'),
			// 		slides_data: [
			// 			{
			// 				image_url: 'http://via.placeholder.com/350x150',
			// 				image_dom: 'test123'
			// 			}
			// 		],
			// 		views: {
			// 			text_container: '<div class="container"><div class="text_container"></div></div>',
			// 		}
			// 	});

			var default_views = {
				slideshow_container: '<div class="slideshow_container"></div>',
				image_container: '<div class="image_container"></div>',
				text_container: '<div class="text_container"></div>'
			};

			var that = this;

			var target_element = (typeof params.target_element === "undefined") ? null : params.target_element;
			var slides_data = (typeof params.slides_data === "undefined") ? null : params.slides_data;
			var views = (typeof params.views === "undefined") ? default_views : params.views;
			views.slideshow_container = (typeof views.slideshow_container === "undefined") ? default_views.slideshow_container : views.slideshow_container;
			views.image_container = (typeof views.image_container === "undefined") ? default_views.image_container : views.image_container;
			views.text_container = (typeof views.text_container === "undefined") ? default_views.text_container : views.text_container;

			var render = {
				slideshow_container: function (argument) {
					var returnObject = $(views.slideshow_container);
					returnObject.append(views.image_container);
					returnObject.append(views.text_container);
					return returnObject;
				},
				slide: function(imageUrl){
					target_element.append(slideshow_container);
					slideshow_container.find('.image_container').bsSlider(imageUrl);
				}
			}

			var init = function(){
				// build slideshow DOM
				var slideshow_object = render.slideshow_container();

				// build image array
				var image_array = _.map(slides_data, function(value_slides_data, index_slides_data){
					return value_slides_data.image_url;
				});

				// add slider to target_element
				target_element.html(slideshow_object);

				// run slider
				target_element.bsSlider(image_array, {
					on_image_change: function (index) {
						if(typeof slides_data[index] !== 'undefined'){
							slideshow_object.find('.text_container').html(_.unescape(slides_data[index].image_dom))
						}
					}
				});
			}

			if(target_element != null && slides_data != null){
				init();
			}
		};

	boilerplate.prototype.renderItems = function(params){

		// USAGE
		// that.renderItems({
		// 	target_element: $('body'),
		// 	elements_to_render: [],
		//  renderFunction: function()
		// });

		var that = this;
		var target_element = (typeof params.target_element === "undefined") ? null : params.target_element;
		var elements_to_render = (typeof params.elements_to_render === "undefined") ? [] : params.elements_to_render;
		var renderFunction = (typeof params.renderFunction === "undefined") ? function(){} : params.renderFunction;

		var init = function(){
			
			_.each(elements_to_render, function(value_elements_to_render, index_elements_to_render){
				renderFunction(value_elements_to_render, target_element);
			})

		}

		init();
	};

	boilerplate.prototype.renderButton = function(params){

		// USAGE
		// var this_button = that.renderButton({
		// 	button_dom: 'test123',
		// 	button_pageID: 'pageID',
		//	button_postID: 'postID',
		//	button_URL: 'http://test.com',
		// 	button_view: '<div class="button" />'
		// });

		var that = this;

		var button_dom = (typeof params.button_dom === "undefined") ? '' : params.button_dom;
		var button_pageID = (typeof params.button_pageID === "undefined") ? null : params.button_pageID;
		var button_postID = (typeof params.button_postID === "undefined") ? null : params.button_postID;
		var button_URL = (typeof params.button_URL === "undefined") ? null : params.button_URL;
		var button_view = (typeof params.button_view === "undefined") ? '<div class="button" />' : params.button_view;

		var bind = {
			click: function(element_to_bind){
				element_to_bind.on('click', function(e){
					e.preventDefault();
					if(typeof $(this).attr('data-postid') !== 'undefined'){
						that.pushPageNav($(this).attr('data-pageid'), $(this).attr('data-postid'))
					} else {
						that.pushPageNav($(this).attr('data-pageid'))
					}
				})
			}
		};

		var render = {
			content: function () {
				var button_element = $(button_view);
				if(button_dom != ''){
					button_element.html(button_dom);
				}
				return button_element;
			},
			attr: {
				external: function (attr_element){
					attr_element.attr({
						'href': button_URL,
						'target': '_blank'
					});
				},
				pageid: function (attr_element) {
					attr_element.attr({
						'data-pageid': button_pageID,
						'href': '/' + button_pageID + '/',
						'target': '_blank'
					});
				},
				pageid_postid: function (attr_element){
					attr_element.attr({
						'data-pageid': button_pageID,
						'data-postid': button_postID,
						'href': '/' + button_pageID + '/' + button_postID + '/',
						'target': '_blank'
					});
				}
			}
		}

		var init = function (argument) {

			var returnObject = render.content();

			if(button_URL != null){

				render.attr.external(returnObject);

			} else {

				if(button_pageID != null && button_postID != null){

					render.attr.pageid_postid(returnObject);
					bind.click(returnObject);

				} else if (button_pageID != null){

					render.attr.pageid(returnObject);
					bind.click(returnObject);

				}

			}

			return returnObject;
		}

		return init();

	};

	boilerplate.prototype.bsSlider = function(imageRequest, params) {		

		var this_element = this;
		var params = (typeof params === "undefined") ? {} : params;
		var ignore_css = (typeof params.ignore_css === "undefined") ? false : params.ignore_css;
		var speed = (typeof params.speed === "undefined") ? 4000 : params.speed;
		var on_image_change = (typeof params.on_image_change === "undefined") ? function(){} : params.on_image_change;

		if($('body').find('.bs_styles').size() == 0){
			var returnObject = $('<div class="bs_styles" />');
			returnObject.html('<style>.hide_top_block{opacity: 0 !important;}</style>')
			$('body').append(returnObject);
		}

		if(typeof this_element.data('bs_slider') === "undefined"){

			var bs_slider = function () {
				var target_element = this_element;
				var image_request;
				var mode;
				var resume_slideshow = function () {
					slideshow.resume_slideshow();
				};
				var stop_slideshow = function () {
					slideshow.stop_slideshow();
				};
				var go_to_prev_slide = function () {
					slideshow.stop_slideshow();
					slideshow.prev_slide();
				};
				var go_to_next_slide = function () {
					slideshow.stop_slideshow();
					slideshow.next_slide();
				};
				var get_current_index = function(){
					return slideshow.current_slide;
				};
				var go_to_slide = function(index_request){
					slideshow.stop_slideshow();
					slideshow.go_to_slide_by_index(index_request);
				}
				var slideshow = {
					_slideshow_interval: null,
					slideshow_target: null,
					current_slide: 0,
					slideshow_speed: speed,
					slide_image_array: null,
					set_params: function (params) {
						var that = this;
						if(typeof params.slideshow_speed !== "undefined"){
							that.slideshow_speed = params.slideshow_speed;
						}
					},
					go_to_slide_by_index: function(slide_index){
						var that = this;
						slide_index = (typeof slide_index === 'undefined') ? 0 : slide_index;
						that.current_slide = slide_index;
						that.show_image(that.slideshow_target, that.slide_image_array[that.current_slide]);
					},
					next_slide: function () {
						var that = this;
						if((that.current_slide + 1) > (that.slide_image_array.length - 1)){
							that.current_slide = 0;

						} else {
							that.current_slide++;
						}
						that.show_image(that.slideshow_target, that.slide_image_array[that.current_slide]);
					},
					prev_slide: function () {
						var that = this;
						if((that.current_slide - 1) < (0)){
							that.current_slide = that.slide_image_array.length - 1;

						} else {
							that.current_slide--;
						}
						that.show_image(that.slideshow_target, that.slide_image_array[that.current_slide]);
					},
					resume_slideshow: function () {
						var that = this;
						that._slideshow_interval = setInterval(function(){
							that.next_slide();
						}, that.slideshow_speed)
					},
					start_slideshow: function(target_element, image_array){
						var that = this;
						that.slide_image_array = image_array;
						that.slideshow_target = target_element;
						that.show_image(that.slideshow_target, that.slide_image_array[that.current_slide]);
						that.resume_slideshow();
					},
					stop_slideshow: function () {
						var that = this;
						clearInterval(that._slideshow_interval);
					},
					create_slide_containers: function (target_element) {
						target_element.css({
							'position': 'relative'
						});
						var image_container_0 = $('<div class="image_blocks top_block" />');
						var default_css = {
							'position': 'absolute',
							'top': '0px',
							'left': '0px',
							'width': '100%',
							'height': '100%',
							'zIndex': '100',
							'opacity': '1',
							'transition': '.4s opacity'
						};
						var css_to_use = (ignore_css) ? {} : default_css;
						image_container_0.css(css_to_use);
						target_element.append(image_container_0)

						var image_container_1 = image_container_0.clone();

						var css_to_use_1 = (ignore_css) ? {} : {
							'zIndex': '99'
						};
						image_container_1.css(css_to_use_1);
						image_container_1.removeClass('top_block');
						image_container_1.addClass('bottom_block');
						target_element.append(image_container_1)
					},
					show_image: function(target_element, image_url){
						var that = this;
						var element_exists = ($(target_element).parents('body').find($(target_element)).size() == 0) ? false : true;
						if(element_exists){
							on_image_change(slideshow.current_slide);
							if($(target_element).find('.image_blocks').size() == 0){
								that.create_slide_containers(target_element);
							}
							var top_block_is_hidden = (target_element.find('.hide_top_block').size() > 0) ? true : false;
							if(top_block_is_hidden){

								target_element.find('.top_block').css({
									'background-image': 'url("' + image_url + '")',
									'background-size': 'cover',
									'background-repeat': 'no-repeat',
									'background-position': '50% 50%'
								});

								target_element.find('.top_block').removeClass('hide_top_block')
							} else {

								target_element.find('.bottom_block').css({
									'background-image': 'url("' + image_url + '")',
									'background-size': 'cover',
									'background-repeat': 'no-repeat',
									'background-position': '50% 50%'
								});

								target_element.find('.top_block').addClass('hide_top_block')
							}
						} else {
							that.stop_slideshow();
						}
					}
				};
				var set_mode = function(mode_request){
					var this_mode;

					if( Object.prototype.toString.call( mode_request ) === '[object Array]' ) {
						if(mode_request.length > 1){
							this_mode = 'multi_image';
						} else {
							this_mode = 'single_image';
						}
					} else {
						this_mode = 'single_image';
					}

					return this_mode;
				};
				var init = function (image_data) {
					image_request = image_data;
					mode = set_mode(image_request);
					switch(mode){

						case 'single_image':
							slideshow.show_image(target_element, image_request);
						break;

						case 'multi_image':
							slideshow.set_params({
								slideshow_speed: speed
							});
							slideshow.start_slideshow(target_element, image_request);
						break;

					}
				};
				return {
					resume_slideshow: resume_slideshow,
					stop_slideshow: stop_slideshow,
					go_to_prev_slide: go_to_prev_slide,
					go_to_next_slide: go_to_next_slide,
					get_current_index: get_current_index,
					go_to_slide: go_to_slide,
					init: init
				}
			};

			var this_bs_slider = new bs_slider();
			this_element.data('bs_slider', this_bs_slider);

			this_bs_slider.init(imageRequest);

		} else {
			switch(imageRequest){
				case 'next':
					var this_bs_slider = this_element.data('bs_slider');
					this_bs_slider.go_to_next_slide()
				break;

				case 'prev':
					var this_bs_slider = this_element.data('bs_slider');
					this_bs_slider.go_to_prev_slide()
				break;

				case 'pause':
					var this_bs_slider = this_element.data('bs_slider');
					this_bs_slider.stop_slideshow()
				break;

				case 'resume':
					var this_bs_slider = this_element.data('bs_slider');
					this_bs_slider.stop_slideshow()
					this_bs_slider.resume_slideshow()
				break;
			}
		}

	}

	boilerplate.prototype.renderBlog = function(){
		var that = this;
		var blogView = $(that.php_blog_item);
		var commentView = blogView.find('.commentItem').eq(0).clone();
		blogView.find('.commentItem').remove();

		var loadComments = function(itemID){
			$.post(that.pageDir + "/machines/handlers/loadComments.php", {'id': itemID}, function(data) {
				var thisBlogItem = $('.blogItem' + itemID);
				thisBlogItem.find('.commentList').html('')
				_.each(data, function(value_data, index_data){
					var thisComment = that.renderModel(value_data, commentView.clone());
					thisBlogItem.find('.commentList').append(thisComment);
				});
			}, "json");
		};

		var renderBlogItem = function(){
			var numbericPostID = postID.split('-');
			numbericPostID = parseInt(numbericPostID[numbericPostID.length - 1]);

			that.returnAndSaveJsonData('listBlogItems', function(blogData){
				var thisBlogItem = that.accessor_listBlogItems[numbericPostID];
				var thisBlogView = that.renderModel(thisBlogItem, blogView.clone());
				thisBlogView.find('.commentForm').data('blogid', thisBlogItem.post_id);
				thisBlogView.addClass('blogItem' + thisBlogItem.post_id);

				thisBlogView.css({
					'border-top': '12px solid black',
					'margin-top': '12px',
					'padding-top': '12px'
				});

				$('.pageSection').html('<div class="container" />');
				$('.pageSection').find('.container').html(thisBlogView);
				
				processCommentForm(thisBlogView.find('.commentForm'), false, loadComments);

				loadComments(thisBlogItem.post_id);
				that.changePage();
			});

		};

		var renderBlogListing = function(){
			that.returnAndSaveJsonData('listPages', function(pagesData){
				thisPageData = that.accessor_listPages[pageID];
				$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner)));
				$('.pageSection').find('.the_content').after('<div class="blogListing" />');

				that.returnAndSaveJsonData('listBlogItems', function(blogData){
					_.each(blogData, function(value_blogData, index_blogData){
						var thisBlogView = that.renderModel(value_blogData, blogView.clone());

						thisBlogView.find('.commentForm').data('blogid', value_blogData.post_id);
						thisBlogView.addClass('blogItem' + value_blogData.post_id);

						var thisPostID = that.slugify(_.unescape(value_blogData.the_title)) + '-' + value_blogData.post_id;
						thisBlogView.find('.the_title').wrap( '<a class="blogLink" href="/' + pageID + '/' + thisPostID + '/"></a>' );
						thisBlogView.find('.blogLink').attr('data-pageid', pageID);
						thisBlogView.find('.blogLink').attr('data-postid', thisPostID);

						thisBlogView.css({
							'border-top': '12px solid black',
							'margin-top': '12px',
							'padding-top': '12px'
						});

						$('.blogListing').append(thisBlogView);
						
						processCommentForm(thisBlogView.find('.commentForm'), false, loadComments);

						loadComments(value_blogData.post_id);
					});

					$('.blogLink').on('click', function(e){
						e.preventDefault();
						that.pushPageNav($(this).attr('data-pageid'), $(this).attr('data-postid'))
					})
				});

				that.changePage();
			});
		};

		var processCommentForm = function (theForm, doCaptcha, callback){
			var doCaptcha = (typeof doCaptcha !== "undefined") ? doCaptcha : false;
			var csvUpdate = (theForm.data('csvupdate') != "") ? theForm.data('csvupdate') : 'formData';

			// you must enter your public key here - https://www.google.com/recaptcha/
			if(doCaptcha){
				Recaptcha.create("6LcgYfASAAAAAAvTvfb5r97NKTNnMbnCUnVN2-bp", theForm.find('.captchaField').attr('id'), {
					tabindex: 1,
					theme: "clean",
					callback: Recaptcha.focus_response_field
				});
			}

			function sendForm(){
				var blogID = theForm.data('blogid');
				$.post(that.pageDir + "/machines/handlers/processCommentForm.php", { formData: formData, blogID: blogID, csvUpdate: csvUpdate }, function(data) {
					callback(blogID);
					if(data == "success"){
						$(".formResponse").html("Form sent!");
						// $(".formResponse").css('color', 'green');
						$(".formResponse").css('display', 'block !important');
	//					$('#contactCaptcha').find('.fieldResponse').html('');
					} else {
						$(".formResponse").html("Form not sent. Please refresh the page and try again");
	//					$(".formResponse").css('color', 'red');
						$(".formResponse").css('display', 'block !important');
					}
				});
			}
		

			theForm.validate({
				errorClass: "formError",
				submitHandler: function(thisForm) {
					formData = theForm.serialize();
					postArgs = {
						challenge: $('#recaptcha_challenge_field').val(),
						response: $('#recaptcha_response_field').val()				
					}

					// you must enter your private key in "/machines/libraries/recaptcha/recaptchaResponse.php" - https://www.google.com/recaptcha/
					if(doCaptcha){
						$.post(pageDir + "/machines/libraries/recaptcha/recaptchaResponse.php", postArgs, function(data){
							if(data.substring(0,4) == "true"){
								sendForm();
							} else {
								Recaptcha.reload()
								returnObject = $('<div class="fieldResponse">Incorrect Captcha, please try again</div>');
								$('#contactCaptcha').append(returnObject)
							}
						})
					} else {
						sendForm();
					}

				},
				invalidHandler: function(event, validator) {
					errors = validator.numberOfInvalids();
					if (errors) {
						message = errors == 1 ? 'You missed 1 field.' : 'You missed ' + errors + ' fields.';
	//					$(".formResponse").html(message);
						$(".formResponse").show();
					} else {
						$(".formResponse").hide();
					}
				},
				errorPlacement: function(error, element) {
					switch(element.attr("name")){
						case "spam":
							$("input[name='spam']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
						break;
						case "transportation":
							$("input[name='transportation']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
						break;
						case "subscribe_email":
							element.parents('fieldset').after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
						break;
						default:
							element.parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
					}
				},
				messages: {
					email: {
						email: "Your email address must be in the format of name@domain.com"
					},
					telephone: {
						phoneUS: "Your phone number must be in the format of 212-555-1000"
					},
				},
				rules: {
					email: {
						required: true,
						email: true
					},
					telephone:{
						required: false,
						phoneUS: true
					},
					comment:{
						required: true
					},
					subscribe_email:{
						required: true,
						email: true
					}
				}
			});
		}

		if(that.postIDnotSet()){
			renderBlogListing();
		} else {
			renderBlogItem();
		}

	}

	boilerplate.prototype.bindMenuAnimations = function(menuTarget){
		menuTarget.children('li').mouseenter(function(){
			var hoverElement = $(this);
			var animationElement = hoverElement.children('ul');

			if(animationElement.size() > 0){

				dynamics.css(animationElement[0], {
					opacity: 0,
					display: 'block',
					scale: 1.1
				})
				dynamics.animate(animationElement[0], {
					scale: 1,
					opacity: 1
				}, {
					change: function(){
						if(typeof hoverElement.firstRun === "undefined" || hoverElement.firstRun == 0){
							hoverElement.firstRun = 1;
							hoverElement.siblings('li').children('ul').css('display','none');

							hoverElement.children('ul').children('li').each(function(index){
								var item = $(this)[0]
								dynamics.css(item, {
									opacity: 0,
									translateY: 20
								})

								dynamics.animate(item, {
									opacity: 1,
									translateY: 0
								}, {
									type: dynamics.spring,
									frequency: 300,
									friction: 435,
									duration: 1000,
									delay: 100 + index * 40
								})
							})

						}
					},
					complete: function(){
					},
					duration: 1000,
					type: dynamics.spring,
					frequency: 300,
					friction: 900,
				})
			}
		}).mouseleave(function(){
			var hoverElement = $(this);
			var animationElement = hoverElement.children('ul');

			if(animationElement.size() > 0){
				dynamics.animate(animationElement[0], {
					opacity: 0
				}, {
					complete: function(){
						animationElement.css('display', 'none');
					},
					delay: 300,
					duration: 300
				})
			}
		});

	};

	boilerplate.prototype.singlePageAnimate = function(pageIDrequest){
		var that = this;
		that.animateSomething({
			target: $('body'),
			animation: {
				scrollTop: $('#page_' + pageIDrequest).offset().top - $('.pageSection').offset().top,
			},
			time: 400,
			callback: function() {
				console.log('page loaded')
			}
		})
	};

	boilerplate.prototype.adminPages = function(currentPage){
		var that = this;
		switch(currentPage){
			case "galleries":
				that.renderGalleriesAdmin();
			break;

			case "page":
			case "events":
				that.featuredImagesLauncher();
			break;
		}
	};

	boilerplate.prototype.render_og_metadata = function(params){
		var that = this;
		var page_title = (typeof params.page_title === 'undefined') ? null : params.page_title;
		var page_content = (typeof params.page_content === 'undefined') ? null : params.page_content;
		var page_image = (typeof params.page_image === 'undefined') ? null : params.page_image;

		var meta_view = '<meta property="" content="" />';

		var meta = {
			url: window.location.href,
			type: 'website',
			title: page_title,
			description: $(_.unescape(page_content)).text(),
			image: page_image
		}

		var render = {
			meta: function(){
				return $(meta_view);
			},
			attr: function (target, value, index) {
				target.attr({
					'content': value,
					'property': 'og:' + index
				})
			}
		}

		var init = function(){
			_.each(meta, function (value_meta_views, index_meta_views) {

				var meta_element = $('head').find('meta[property="og:' + index_meta_views + '"]');

				if(meta_element.length == 0){

					var meta_item = render.meta();
					render.attr(meta_item, value_meta_views, index_meta_views)
					$('head').append(meta_item);

				} else {

					render.attr(meta_element, value_meta_views, index_meta_views)

				}

			})

		}

		init();
	};

	boilerplate.prototype.multiPageSaveAjax = function(params){

		// SETUP
			// Update /machines/handlers/saveAjax.php with $rootURL = '/var/www/html/wp-content/themes/YOUR-WEBSITE-DIRECTORY/';
			// Update /machines/custom-functions.php in the function my_page_template_redirect() to $defaultPageID = YOUR_DEFAULT_PAGE_SLUG;
			// Update models and pages in multiPageSaveAjax
			// Choose mode for output of data xml or pages ... defaults to xml

		// XML
			// In the console Right-Click > Copy > Outer HTML and paste code into new file
			// Save to root directory as sitemap.xml

			// USAGE
				/* 
					webApp.multiPageSaveAjax({
						mode: 'xml',
						models: [
						'listPages',
						'listProjectTypes',
						'listProjects',
						'listServices',
						'listContentBoxes',
						'listJobs'
						],
						pages: {
							'home': {},
							'about': {},
							'our-team': {},
							'services': {},
							'portfolio': {},
							'projects': {
								postID: {
									model: 'listProjects',
									post_id_build: function(project_data){
										var project_title = _.unescape(project_data.the_title);
										var project_id = project_data.post_id;
										return webApp.slugify(project_title)
									}
								}
							},
							'careers': {},
							'contact-us': {}
						}
					}); 
				*/

		// PAGES
			// Verify pages rendered by checking in /var/www/html/wp-content/themes/YOUR-WEBSITE-DIRECTORY/_ajax
			// Commit to git

			// USAGE
				// See above usage example
				// Change mode to 'pages'

		var that = this;
		var models = params.models;
		var pages = params.pages;
		var mode = (typeof params.mode !== 'undefined') ? params.mode : 'xml';
		var data = {};
		var get_model_data = function (done_func, models_to_fetch) {
			if(typeof models_to_fetch === 'undefined'){
				models_to_fetch = models;
			}
			var this_model_name = models_to_fetch[0];

			that.returnAndSaveJsonData(this_model_name, function (this_model_data) {
				data[this_model_name] = this_model_data;

				if(models_to_fetch.length <= 1){
					done_func();
				} else {
					var new_array = models_to_fetch.slice(1, models_to_fetch.length);
					get_model_data(done_func, new_array);
				}
			})
		}
		var build_page_array = function () {
			var final_array = [];
			_.each(pages, function (value_pages, index_pages) {
				var this_page_object = {
					pageID: index_pages,
					postID: null
				}
				if(typeof value_pages.postID !== 'undefined'){
					var this_post_id_name = value_pages.postID.model;
					var this_post_id_build = value_pages.postID.post_id_build;
					var this_post_id_data = data[this_post_id_name];
					_.each(this_post_id_data, function (value_this_post_id_data, index_this_post_id_data) {
						var this_page_object = {
							pageID: index_pages,
							postID: this_post_id_build(value_this_post_id_data)
						}
						final_array.push(this_page_object)
					})
				}
				final_array.push(this_page_object)
			})
			return final_array;
		}
		var build_xml = function (finalArray) {
			var xml_output = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
			_.each(finalArray, function(value_finalArray, index_finalArray){
				var pageString = "";
				if(value_finalArray.postID == null){
					pageString = value_finalArray.pageID + '/';
				} else {
					pageString = value_finalArray.pageID + '/' + value_finalArray.postID + '/';
				}
				var this_xml_item = '<url><loc>' + that.pathPrefix + pageString + '</loc><changefreq>weekly</changefreq></url>';
				xml_output += this_xml_item;
			});
			xml_output += '</urlset>';
			return xml_output;
		}

		var recursiveSEO = function () {

			var page_array_index = 0;
			var time_to_wait_on_page = 5000;

			var initialize = function (this_list_of_pages) {
				window['list_of_pages'] = this_list_of_pages;
				recursive_page_change()
			}

			var recursive_page_change = function(){
				// log the current number until it reaches our logic constraint
				var this_page_item = window['list_of_pages'][page_array_index];
				var newPostID = (this_page_item.postID != null) ? this_page_item.postID : "";
				that.pushPageNav(this_page_item.pageID, newPostID);
				// increment the default number
				page_array_index++;
				setTimeout(function(){
					// check if the test number is less than 100 AND less then our array length
					that.saveAjax();
					if(page_array_index < window['list_of_pages'].length){
						recursive_page_change();
					}
				}, time_to_wait_on_page);
			}

			var debug0 = function(value){
				console.log(value);
				$('#container').html(value);
			}

			return {
				initialize: initialize
			}

		}

		var init = function (argument) {
			get_model_data(function (argument) {
				
				switch(mode){
					case 'pages':
						var pages_array = build_page_array();
						var this_seo_instance = new recursiveSEO();
						this_seo_instance.initialize(pages_array);
					break;

					case 'xml':
						var pages_array = build_page_array();
						var xml_data = build_xml(pages_array);
						console.log(xml_data)
					break;
				}
			})
		}
		init();
	};
	

	boilerplate.prototype.render_user_defined_function = function (params) {

		var that = this;
		var data = params.data;
		var view = params.view;
		if(typeof that[data.cb_function] !== 'undefined'){
			that[data.cb_function](data, view);
		} else {
			// console.log(data, params.view);
		}

	}

	boilerplate.prototype.renderContentBoxes = function (params) {
		var that = this;

		var default_view = '<div class="content_box"><div class="the_title"></div></div>';

		var target_element = (typeof params.target_element === "undefined") ? null : params.target_element;
		var current_page = (typeof params.current_page === "undefined") ? pageID : params.current_page;
		var on_element_render = (typeof params.on_element_render === "undefined") ? function(){} : params.on_element_render;
		var content_boxes_model = (typeof params.content_boxes_model === "undefined") ? 'listContentBoxes' : params.content_boxes_model;
		var pages_model_cb_field = (typeof params.pages_model_cb_field === "undefined") ? 'pages_content_boxes' : params.pages_model_cb_field;
		var cb_model_style_field = (typeof params.cb_model_style_field === "undefined") ? 'cb_layout' : params.cb_model_style_field;
		var cb_model_bg_color_field = (typeof params.cb_model_style_field === "undefined") ? 'cb_bg_color' : params.cb_model_style_field;

		var views = (typeof params.views === "undefined") ? that.getCBviews() : params.views;

		var render = {
			content_box: function(cb_data, cb_view, done_function){
				var returnObject = that.renderModel(cb_data, $(cb_view));
				target_element.append(returnObject);
				returnObject.addClass(that.slugify(_.unescape(cb_data.the_title)));
				this.style.bgColor(returnObject, cb_data[cb_model_bg_color_field]);
				this.button(returnObject, cb_data);

				that.render_user_defined_function({
					data: cb_data,
					view: returnObject
				});

				done_function(cb_data, returnObject);
				return returnObject;
			},
			button: function(cb_object, data){
				var cb_page_text = (typeof data.cb_page_text !== 'undefined' && data.cb_page_text != '') ? data.cb_page_text : null;
				var cb_page_link = (typeof data.cb_page_link !== 'undefined' && data.cb_page_link != '') ? data.cb_page_link : null;

				if(cb_page_text != null && cb_page_link != null){
					var this_button = that.renderButton({
						button_dom: cb_page_text,
						button_pageID: that.slugify(_.unescape(that.accessor_ids_listPages[cb_page_link].the_title)),
					});
					cb_object.find('.button_container').append(this_button);
				}

			},
			style: {
				bgColor: function(cb, bgColor){
					if(bgColor != ''){
						cb.css('background-color', bgColor)
					}
				}
			}
		}

		var init = function(){
			that.get.data.call(that, [content_boxes_model], function(data){
				var this_pages_content_boxes = that.accessor_listPages[current_page][pages_model_cb_field];
				_.each(this_pages_content_boxes, function(value_this_pages_content_boxes, index_this_pages_content_boxes){
					var cb_data = that['accessor_' + content_boxes_model][value_this_pages_content_boxes];
					if(typeof cb_data !== 'undefined'){
						var cb_view = (typeof views[cb_data[cb_model_style_field]] === 'undefined') ? default_view : views[cb_data[cb_model_style_field]];
						var this_content_box = render.content_box(cb_data, cb_view, on_element_render);
					}
					
				});
			});
		}

		if(content_boxes_model != null && pages_model_cb_field != null && target_element != null){
			init();
		}
	}

	boilerplate.prototype.processForm = function (params){
		
		var defaults = {
			rules: {
				email: {
					required: true,
					email: true
				},
				telephone:{
					required: false,
					phoneUS: true
				},
				comment:{
					required: true
				},
				subscribe_email:{
					required: true,
					email: true
				}
			},
			messages: {
				email: {
					email: "Your email address must be in the format of name@domain.com"
				},
				telephone: {
					phoneUS: "Your phone number must be in the format of 212-555-1000"
				},
			},
			errorPlacement: function(error, element) {
				switch(element.attr("name")){
					case "spam":
						$("input[name='spam']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
					break;
					case "transportation":
						$("input[name='transportation']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
					break;
					case "subscribe_email":
						element.parents('fieldset').after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
					break;
					default:
						element.parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
				}
			},
			invalidHandler: function(event, validator) {
				errors = validator.numberOfInvalids();
				if (errors) {
					message = errors == 1 ? 'You missed 1 field.' : 'You missed ' + errors + ' fields.';
					$(".formResponse").html(message);
					$(".formResponse").show();
				} else {
					$(".formResponse").hide();
				}
			},
			submitHandler: function(thisForm) {
				formData = theForm.serialize();
				postArgs = {
					challenge: $('#recaptcha_challenge_field').val(),
					response: $('#recaptcha_response_field').val()				
				}

				if(doCaptcha){
					$.post(pageDir + "/machines/libraries/recaptcha/recaptchaResponse.php", postArgs, function(data){
						if(data.substring(0,4) == "true"){
							sendForm();
						} else {
							Recaptcha.reload()
							returnObject = $('<div class="fieldResponse">Incorrect Captcha, please try again</div>');
							$('#contactCaptcha').append(returnObject)
						}
					})
				} else {
					sendForm();
				}

			},
			errorClass: "formError",
			sendForm: function(){
				$.post(that.pageDir + php_url, { formData: formData, csvUpdate: csvUpdate }, function(data) {
					callback(data);
					if(data == "success"){
						theForm.find(".formResponse").html("Form sent!");
						theForm.find(".formResponse").css('display', 'block !important');
					} else {
						theForm.find(".formResponse").html("Form not sent. Please refresh the page and try again");
						theForm.find(".formResponse").css('display', 'block !important');
					}
				});
			},
			recaptcha_key: "6LcgYfASAAAAAAvTvfb5r97NKTNnMbnCUnVN2-bp",
			php_url: "/machines/handlers/processForm.php"
		}

		var that = this;
		var theForm = (typeof params.theForm !== "undefined") ? params.theForm : false;
		var doCaptcha = (typeof params.doCaptcha !== "undefined") ? params.doCaptcha : false;
		var callback = (typeof params.callback !== "undefined") ? params.callback : function(){};
		var php_url = (typeof params.php_url !== "undefined") ? params.php_url : defaults.php_url;
		var rules = (typeof params.rules !== "undefined") ? params.rules : defaults.rules;
		var messages = (typeof params.messages !== "undefined") ? params.messages : defaults.messages;
		var errorPlacement = (typeof params.errorPlacement !== "undefined") ? params.errorPlacement : defaults.errorPlacement;
		var invalidHandler = (typeof params.invalidHandler !== "undefined") ? params.invalidHandler : defaults.invalidHandler;
		var errorClass = (typeof params.errorClass !== "undefined") ? params.errorClass : defaults.errorClass;
		var sendForm = (typeof params.sendForm !== "undefined") ? params.sendForm : defaults.sendForm;
		var submitHandler = (typeof params.submitHandler !== "undefined") ? params.submitHandler : defaults.submitHandler;
		var recaptcha_key = (typeof params.recaptcha_key !== "undefined") ? params.recaptcha_key : defaults.recaptcha_key;

		var csvUpdate = (theForm.data('csvupdate') != "") ? theForm.data('csvupdate') : 'formData';

		if(doCaptcha){
			Recaptcha.create(recaptcha_key, theForm.find('.captchaField').attr('id'), {
				tabindex: 1,
				theme: "clean",
				callback: Recaptcha.focus_response_field
			});
		}	

		theForm.validate({
			errorClass: errorClass,
			submitHandler: submitHandler,
			invalidHandler: invalidHandler,
			errorPlacement: errorPlacement,
			messages: messages,
			rules: rules
		});

	}

})(jQuery);