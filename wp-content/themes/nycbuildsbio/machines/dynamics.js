(function($){

	$(document).ready(function(){

		webApp = new boilerplate('webApp');

		if(!webApp.ajaxer){
			webApp.start({
				singlePage: false,
				gaAnalyticsID: 'UA-135906261-1',
				defaultPage: 'home',
				RewriteBase: "/",
				themeFolderName: 'nycbuildsbio',
				typekitID: null,
				skip_animation_pages: ['projects', 'leadership'],
				filesToVariablesArray: [
					{'page_inner': 'views/page_inner.php'},
					{'callout_box_title_bg': 'views/output_callout_box_title_bg.php'},
					{'callout_box_title_side': 'views/output_callout_box_title_side.php'},
					{'text_1_col_title_side': 'views/output_text_1_col_title_side.php'},
					{'text_1_col_title_top': 'views/output_text_1_col_title_top.php'},
					{'text_1_col_title_bg': 'views/output_text_1_col_title_bg.php'},
					{'text_2_col_title_side': 'views/output_text_2_col_title_side.php'},
					{'text_2_col_no_title': 'views/output_text_2_col_no_title.php'},
					{'text_2_col_title_side_66_33': 'views/output_text_2_col_title_side_66_33.php'},
					{'map': 'views/output_map.php'},
					{'content_100': 'views/output_content_100.php'},
					{'slideshow_big': 'views/output_slideshow_big.php'},
					{'person_item': 'views/output_person_item.php'},
					{'event_person_item': 'views/output_event_person_item.php'},
					{'person_item_bio': 'views/output_person_item_bio.php'},
					{'blockquote_slideshow': 'views/output_blockquote_slideshow.php'},
					{'testimonial_item': 'views/output_testimonial_item.php'},
					{'sponsor_item': 'views/output_sponsor_item.php'},
					{'event_item': 'views/output_event_item.php'},
					{'past_event_item': 'views/output_past_event_item.php'},
					{'news_item': 'views/output_news_item.php'},
					{'contact_map': 'views/output_contact_map.php'},
					{'slick_arrow': 'views/output_slick_arrow.php'},
					{'event_calendar_item': 'views/output_event_calendar_item.php'},
					{'month_container': 'views/output_month_container.php'},
					{'member_benefits_form': 'views/output_member_benefits_form.php'},
					{'gallery_listing_item': 'views/output_gallery_listing_item.php'},
					{'event_sponsors_listing': 'views/output_event_sponsors_listing.php'},
					{'event_sponsor_item': 'views/output_event_sponsor_item.php'},
					{'person_item_popup': 'views/output_person_item_popup.php'},
					{'page_gallery_item': 'views/page_gallery_item.php'},
					{'page_events_item': 'views/page_events_item.php'}

				],
				viewRender: {
					"gallery": function(thisThat){
						var that = thisThat;
						that.get.data.call(that, ['listPages', 'listGalleries'], function(data){
							var thisPageData = that.accessor_listPages[pageID];
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_inner));
							$('.pageSection').html(thisPageObject);

							if(that.postIDnotSet()){
								that.renderContentBoxes({
									target_element: thisPageObject.find('.content_boxes'),
									on_element_render: function(data, element) {
										switch(data.cb_layout) {
											case 'slideshow_big':
												that.renderSlideshowBig(data, element);
											break;
										}

										that.renderBlockquoteSlideshow({
											blockquote_parent: element,
											secondary_color: data.cb_bg_color_2
										})

									}
								});
								that.changePage();
							} else {
								that.renderGalleryItem({
									done_function: function(){
										that.changePage();
									},
									target_element: thisPageObject.find('.content_boxes')
								});
							}

						})
					},
					"events-test": function(thisThat){
						var that = thisThat;
						// that.returnAndSaveJsonData('listPages', function(pagesData){
						that.get.data.call(that, ['listPages', 'listEvents'], function(data){	

							var thisPageData = that.accessor_listPages[pageID];
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_inner));

							$('.pageSection').html(thisPageObject);

							if(that.postIDnotSet()){
								that.renderContentBoxes({
									target_element: thisPageObject.find('.content_boxes'),
									on_element_render: function(data, element) {
										switch(data.cb_layout) {
											case 'slideshow_big':
												that.renderSlideshowBig(data, element);
											break;
										}

										that.renderBlockquoteSlideshow({
											blockquote_parent: element,
											secondary_color: data.cb_bg_color_2
										})

									}
								});

								that.changePage();
							} else {
								that.renderPastEventItemPage({
									target_element: thisPageObject.find('.content_boxes'),
									data: that.accessor_slug_listEvents[postID],
									done_function: function(){
										that.changePage();
									}
								})
							}
							
						});
					},
					"default": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){

							var thisPageData = that.accessor_listPages[pageID];
							var thisPageObject = that.renderModel(thisPageData, $(that.php_page_inner));

							$('.pageSection').html(thisPageObject);

							that.renderContentBoxes({
								target_element: thisPageObject.find('.content_boxes'),
								on_element_render: function(data, element) {
									switch(data.cb_layout) {
										case 'slideshow_big':
											that.renderSlideshowBig(data, element);
										break;
									}

									that.renderBlockquoteSlideshow({
										blockquote_parent: element,
										secondary_color: data.cb_bg_color_2
									})

								}
							});

							that.changePage();
							
						});
					}
					// FOR BLOG RENDERING
					// "blog": function(thisThat){
					// 	var that = thisThat;
					// 	that.renderBlog();
					// }
					// FOR SINGLE PAGE RENDERING
					// "default": function(thisThat, pageIDrequest){
					// 	var that = thisThat;
					// 	that.returnAndSaveJsonData('listPages', function(pagesData){
					// 		thisPageData = that.accessor_listPages[pageIDrequest];
					// 		pageObject = that.renderModel(thisPageData, $(that.php_page_inner));
					// 		pageObject.attr('id', 'page_' + pageIDrequest)
					// 		$('.pageSection').append(pageObject);
					// 	});
					// }
				},
				helpers: {
					render_og_metadata: function(params){
						// SET PARAMS
							var that = this;
							var this_page_data = that.accessor_listPages[pageID];

						// SET DEFAULT PARAMS
							var default_params = {
								page_title: this_page_data.the_title,
								page_content: $(_.unescape(this_page_data.the_content)).text(),
								page_image: this_page_data.featuredImage
							}

						// SET META DATA VIEW
							var meta_view = '<meta property="" content="" />';

						// SET META DATA OBJECT
							var build = {
								meta_object: function(){
										var meta = {
											url: window.location.href,
											type: 'website'
										};

									// IF PARAMS ARE NOT DEFINED, USE DEFAULT PARAMS
									// OTHERWISE, USE PARAMS PASSED IN	
										if(typeof params === 'undefined'){
											meta.title = default_params.page_title;

											if(!_.isEmpty(default_params.page_content)){
												meta.description = default_params.page_content;
											}

											if(default_params.page_image !== null){
												meta.image = default_params.page_image;
											}
										} else {
											_.each(params, function(value_params, index_params){
												meta[index_params] = value_params;
											})
										}

										return meta;
								}
							}

						// DESTROY
							var destroy = {
								meta_tags: function(){
									var meta_element = $('head').find('meta[property^="og:"]');

									_.each(meta_element, function(value_meta_element, index_meta_element){
										value_meta_element.remove();
									})
								}
							}	

						// RENDER META
							var render = {
								meta: function(){
									return $(meta_view);
								},
								attr: function (target, value, index) {
									target.attr({
										'content': value,
										'property': 'og:' + index
									})
								}
							}

						// INIT
							var init = function(){
								// DESTROY ALL META TAGS
									destroy.meta_tags();

								// BUILD META OBJECT
									var meta = build.meta_object();

								// LOOP THROUGH ALL ELEMENTS OF META OBJECT AND RENDER A META TAG FOR EACH ONE	
									_.each(meta, function (value_meta_views, index_meta_views) {
										var meta_element = $('head').find('meta[property="og:' + index_meta_views + '"]');

										if(meta_element.length == 0){

											var meta_item = render.meta();
											render.attr(meta_item, value_meta_views, index_meta_views)
											$('head').append(meta_item);

										} else {

											render.attr(meta_element, value_meta_views, index_meta_views)

										}

									})

							}

							init();
					},
					renderPastEventItemPage: function(params){
						var that = this;
						var target_element = (typeof params.target_element === 'undefined') ? null : params.target_element;
						var data = (typeof params.data === 'undefined') ? null : params.data;
						var done_function = (typeof params.done_function === 'undefined') ? function(){} : params.done_function;
			

						that.renderItems({
							done_function: function(){
								done_function()
							},
							target_element: target_element,
							elements_to_render: [data],
							renderFunction: function(data, target_element){
								var returnObject = that.renderModel(data, $(that.php_page_events_item));
								target_element.html(returnObject)

								//bg image
								var imageTmp = document.createElement("img");
								imageTmp.setAttribute('class', 'image');
								imageTmp.src = data.featuredImage;
								returnObject.find('.banner_image_container').html(imageTmp)
					
								//returnObject.find('.banner_image_container').bsSlider(data.featuredImage);
								returnObject.find('.content_container').html(_.unescape(data.the_content))


								// CHECKS FOR DATA IN THE SIDEBAR BUTTONS CONTENT AND REMOVES BUTTON IF EMPTY
								// console.log(data)
								if (data.event_agenda === '') {
									returnObject.find('.agenda').addClass('displayNone')
								}
								if (data.event_videos_right === '') {
									returnObject.find('.videos').addClass('displayNone')
								}


								var title = data.the_title

								


								var actions = {
									check_if_there_are_sponsors: function() {
										that.get.data.call(that, ['listSponsorTypes', 'listSponsors'], function(data){
											if (that.accessor_slug_listEvents[postID].event_sponsor_types === ''){
												returnObject.find('.sponsors').addClass('displayNone')
											}
										})
									},
									check_if_there_are_images: function() {
										that.get.data.call(that, ['listGalleries'], function(data){
											if (!that.accessor_slug_listGalleries[postID]) {
												returnObject.find('.gallery').addClass('displayNone')
											}
										})
									},
									clear_content_container: function(){
										returnObject.find('.content_container').html('');
									},
									update_about: function(){
										actions.clear_content_container();
										returnObject.find('.content_container').html(_.unescape(data.the_content));
									},
									update_agenda: function(){
										actions.clear_content_container();
										returnObject.find('.content_container').html(_.unescape(data.event_agenda));
									},
									update_videos: function(){
										actions.clear_content_container();
										returnObject.find('.content_container').append('<blockquote class="videos_blockquote"></blockquote>')
										returnObject.find('.videos_blockquote').html(_.unescape(data.event_videos_right));
									}
								}

								actions.check_if_there_are_sponsors();
								actions.check_if_there_are_images();

								var render = {
									speakers: function(params){
										var target_element = (typeof params.target_element === 'undefined') ? null : params.target_element;
										// actions.clear_content_container();
										
										that.get.data.call(that, ['listPeople', 'listSpeakerTypes'], function(data){
											that.renderItems({
												done_function: function(){
													done_function()
												},
												target_element: target_element,
												elements_to_render: _.filter(data.listPeople, function(data) {
													var isValid = false;



													if(data.person_speaker_event.indexOf(that.accessor_slug_listSpeakerTypes[postID].post_id.toString()) != -1){
														isValid = true;
													} 
													// return data.person_speaker_event == that.accessor_slug_listSpeakerTypes[postID].post_id;
													return isValid;
												}),
												renderFunction: function(data, target_element){
													var returnObject = that.renderModel(data, $(that.php_event_person_item));
													target_element.append(returnObject);

													// SYMPOSIUM EVENT ADDITIONAL SPEAKERS MESSAGE 
													// if ((title == 'Life Sciences 2019 Real Estate Development Symposium') && ($('.additional').length === 0)) {
													// 	target_element.before('<p class="additional">*Speakers to be announced</p>')
													// }

													var bind = {
														popup: function(params){
															params.popupElement.off('click');
															params.popupElement.on('click', function(e){
																e.preventDefault();

																var returnObject = that.renderModel(params.data, $(that.php_person_item_popup));

																$.magnificPopup.open({
																	mainClass: "modal_item",
																	items: {
																		src: returnObject,
																		type: 'inline'
																	},
																	callbacks: {
																		open: function(){
																			returnObject.find('.person_headshot').bsSlider(params.data.featuredImage)
																		}
																	}
																});

															});
														}
													}

													returnObject.find('.person_image').bsSlider(data.featuredImage)
													

													bind.popup({
														popupElement: returnObject.find('.button'),
														data: data
													});
												}
											});

										});
									},
									sponsors: function(params){
										var target_element = (typeof params.target_element === 'undefined') ? null : params.target_element;
										// actions.clear_content_container();
										that.get.data.call(that, ['listEvents', 'listSponsorTypes', 'listSponsors'], function(data){
											// _.each(data.listEvents, function(value_listEvents, index_listEvents){
												// console.log(value_listEvents.event_sponsor_types)
												_.each(that.accessor_slug_listEvents[postID].event_sponsor_types, function(value_sponsor_type, index_sponsor_type){
													that.renderItems({
														done_function: function(){
															done_function()
														},
														target_element: target_element,
														elements_to_render: [that.accessor_listSponsorTypes[parseInt(value_sponsor_type)]],
														renderFunction: function(data, target_element){
															var returnObject = that.renderModel(data, $(that.php_event_sponsors_listing));
															target_element.append(returnObject);
		

															_.each(that.returnAndSaveJsonData('listSponsors'), function(value_sponsors, index_sponsors){
																var value_id = value_sponsors.post_id.toString()
																if ($.inArray(value_id, data.st_sponsors) !== -1) {

																	that.renderItems({
																		done_function: function(){
																			done_function()
																		},
																		target_element: returnObject.find('.sponsors_listing'),
																		elements_to_render: [that.accessor_listSponsors[value_id]],
																		renderFunction: function(data, target_element){
																			var returnObject = that.renderModel(data, $(that.php_event_sponsor_item));
																			target_element.append(returnObject);

																			returnObject.find('.image_container').bsSlider(data.featuredImage);
																			returnObject.find('.logo_link').attr({
																				'href': data.client_url,
																				'target': '_blank'
																			})

																		}
																	});	

																}
															})
				
														}
													});

												})

											// })

										});
									}
								}


								returnObject.find('.about').on('click', function(e){
									e.preventDefault();
									actions.update_about();
								})


								if ( ! ( data.the_title == 'Life Sciences 2020 Real Estate Development Symposium') ) {
									returnObject.find('.agenda').on('click', function(e){
										e.preventDefault();
										actions.update_agenda();
									})
								}
					

								returnObject.find('.videos').on('click', function(e){
									e.preventDefault();
									actions.update_videos();
								})

								returnObject.find('.gallery').attr({
									'data-pageid': 'gallery',
									'data-postid': that.slugify(_.unescape(data.the_title)),
									'href': '/gallery/' + that.slugify(_.unescape(data.the_title)) + '/',
									'target': '_blank'
								})

								returnObject.find('.speakers').on('click', function(e){
									e.preventDefault();
									actions.clear_content_container();
									returnObject.find('.content_container').append('<h1>Speakers</h1>')
									returnObject.find('.content_container').append('<div class="speakers_container"></div>')
									render.speakers({
										target_element: returnObject.find('.speakers_container')
									})
									if ((title == 'Life Sciences 2019 Real Estate Development Symposium') && ($('.additional').length === 0)) {
										returnObject.find('.speakers_container').before('<p class="additional">*Speakers to be announced</p>')
									}
								})

								returnObject.find('.sponsors').on('click', function(e){
									e.preventDefault();
									actions.clear_content_container();
									returnObject.find('.content_container').append('<h1>Sponsors</h1>')
									render.sponsors({
										target_element: returnObject.find('.content_container')
									})
								})

						
								
								returnObject.find('.stickySidebar').sticky({
									topSpacing: $('.header').height() * 3,
									bottomSpacing: 100
								});


								if (( data.the_title == 'Life Sciences 2020 Real Estate Development Symposium')) {
						
							
									returnObject.find('.register.sidebar_button').removeClass('displayNone')
							
									returnObject.find('.agenda.sidebar_button').attr('href', 'http://www.nycbuildsbio.org/wp-content/uploads/2020/12/NYC-2020-Symposium.Agenda.FINAL_.pdf')
									returnObject.find('.agenda.sidebar_button').attr('target', '_blank')

								}
					
							}
						});

				
					},
					renderGalleryItem: function(params){
						var that = this;
						var done_function = (typeof params.done_function === "undefined") ? function(){} : params.done_function;
						var target_element = (typeof params.target_element === "undefined") ? null : params.target_element;

						that.get.data.call(that, ['listGalleries'], function(data){

							that.renderItems({
								done_function: function(){
									done_function()
								},
								target_element: target_element,
								elements_to_render: [that.accessor_slug_listGalleries[postID]],
								renderFunction: function(data, target_element){
									var returnObject = that.renderModel(data, $(that.php_page_gallery_item));
									target_element.html(returnObject);

									that.render_lazy_load_popup_gallery({
										target_element: target_element.find('.image_grid_container'),
										image_string: data.galleries_gallery_image_array
									});
								}
							});

						});
					},
					renderGalleryListing: function(data, view){
						var that = this;
						
						that.get.data.call(that, ['listGalleries'], function(fetchedData){
							that.renderItems({
								target_element: view.find('.extra_dom'),
								elements_to_render: fetchedData.listGalleries,
								renderFunction: function(data, view){
									var returnObject = that.renderModel(data, $(that.php_gallery_listing_item));
									view.append(returnObject)

									var page_id = 'gallery';
									var post_id = that.slugify(_.unescape(data.the_title));

									returnObject.find('.gallery_listing_item_link').attr({
										'data_pageid': page_id,
										'data_postid': post_id,
										'href': '/' + page_id + '/' + post_id + '/'
									});

									that.get.images_data.call(that, data.galleries_gallery_image_array.split(','), function(data){
										if(data.length > 0){
											returnObject.find('.image_container').bsSlider(data[0]);
										}
									});
								}
							});
						});
					},
					render_lazy_load_popup_gallery: function (params) {
						var that = this;
						var target_element = params.target_element;
						var image_string = params.image_string;

						var page_image_id_array = that.get_images_array(image_string);
						that.get_images_data(page_image_id_array, function (data) {

							var thumb_img_array = [];
							var full_img_array = [];
							_.each(page_image_id_array, function (value_page_image_id_array) {
								thumb_img_array.push({
									src: data[value_page_image_id_array].image_data.medium
								})
								full_img_array.push({
									src: data[value_page_image_id_array].image_data.full
								})
							});

							that.imageDrop({
								target: target_element,
								thumb_img_array: thumb_img_array,
								full_img_array: full_img_array,
								image_layout: {
									992: {
										items_per_row: 4,
										ratio_w: 1,
										ratio_h: 1
									},
									768: {
										items_per_row: 2,
										ratio_w: 1,
										ratio_h: 1
									},
									480: {
										items_per_row: 1,
										ratio_w: 1,
										ratio_h: 1
									}
								}
							})

						});
					},
					get_images_data: function(image_id_array, callback){
						var that = this;
						$.post(that.pageDir + '/machines/handlers/loadPost.php', {'postRequest': image_id_array}, function(images_data){
							callback(images_data);
						}, 'json');
					},
					get_images_array: function(image_id_string){
						var sanitized_image_string = (image_id_string.charAt(0) != ',') ? image_id_string : image_id_string.substr(1);
						return sanitized_image_string.split(',');
					},
					imageDrop: function (params){

						var default_layout = {
							0: {
								items_per_row: 1,
								ratio_w: 1,
								ratio_h: 1
							}
						};

						var target = params.target;
						var thumb_img_array = params.thumb_img_array;
						var full_img_array = params.full_img_array;
						var image_layout = (typeof params.image_layout === 'undefined') ? default_layout : params.image_layout;
						image_layout[0] = (typeof image_layout[0] === 'undefined') ? default_layout[0] : image_layout[0];

						var imageDropContainer = $('<div class="imageDropContainer" />');
						target.append(imageDropContainer);

						var add_styles = function(){
							var style_string = '<style>';
							_.each(image_layout, function (value_image_layout, index_image_layout) {
								var items_per_row = value_image_layout.items_per_row;
								var ratio_w = value_image_layout.ratio_w;
								var ratio_h = value_image_layout.ratio_h;
								var min_pixel_size = index_image_layout;
								var item_width = (100/items_per_row).toString() + '%';
								var padding_top = ((ratio_h/ratio_w) * 100).toString() + '%';
								style_string += '@media screen and (min-width:' + min_pixel_size + 'px){.imageDropContainer .imageDropItem{float:left;width:' + item_width + ';padding: 5px;}.imageDropContainer:after{clear:both;content:"";display:block}.imageDropContainer .imageDropItem .fillContainer{padding-top:' + padding_top + ';position:relative}.imageDropContainer .imageDropItem .fillContainer a{display:block;position:absolute;top:0;bottom:0;left:0;right:0}.imageDropContainer .imageDropItem .fillContainer a .image_block{height:100%}}';
							});
							style_string += '</style>';
							imageDropContainer.append($(style_string));
						};

						var build_dom = function(){

							$.each(thumb_img_array, function(index_thumb_img_array, value_thumb_img_array){

								var returnObject = $('<div class="imageDropItem"><div class="imageDropHide fillContainer"><a href=""><div class="image_block"></div></a></div></div>');
								var image_url = value_thumb_img_array.src;

								returnObject.attr({
									'data-src': image_url
								});

								imageDropContainer.append(returnObject);
							});

						};
						var bind_events = function(){

							var check_dom = function(){
								var some_var = [];

								imageDropContainer.find('.imageDropItem').each(function(){
									var this_item = $(this);
									var this_items_offset_top = this_item.offset().top;
									var current_window_scroll_top = $(window).scrollTop();
									var current_window_height = $(window).height();
									var range_min = current_window_scroll_top;
									var range_max = current_window_scroll_top + current_window_height;
									if(this_items_offset_top < range_max && this_items_offset_top > range_min){
										some_var.push(this_item);
									}
								});

								return some_var;
							}
							var showTheseItems = function(){
								var items_on_screen = check_dom();

								$.each(items_on_screen, function(index_items_on_screen, value_items_on_screen){

									var element_to_modify = value_items_on_screen.find('.imageDropHide');
									var this_image_source = value_items_on_screen.attr('data-src');

									if(element_to_modify.hasClass('imageDropHide')){
										element_to_modify.removeClass('imageDropHide');
										element_to_modify.find('a .image_block').bsSlider(this_image_source)
										element_to_modify.find('a').attr('href', this_image_source);

									}

								});
							};

							$(window).off('scroll.imageDrop');
							$(window).on('scroll.imageDrop', function(){
								if(target.parents('body').length == 0){
									$(window).off('scroll.imageDrop');
								} else {
									showTheseItems();
								}
							});
							showTheseItems();

							var popup_gallery_init = function(){
								imageDropContainer.find('.imageDropItem').off('click')
								imageDropContainer.find('.imageDropItem').on('click', function (e) {
									e.preventDefault();
									var this_image_index = $(this).index('.imageDropItem');

									$.magnificPopup.open({
										items: full_img_array,
										type: 'image',
										gallery:{
											enabled: true
										}
									}, this_image_index);

								});
							}

							popup_gallery_init();


						};

						var init = function(){
							add_styles();
							build_dom();
							bind_events();
						};

						init();


					},
					renderMemberBenefitsForm: function(data, view){
						var that = this;

						var member_benefits_form = $(that.php_member_benefits_form);
						view.find('.extra_dom').append(member_benefits_form)

						that.renderForm({
							targetElement: view.find('.extra_dom'),
							data: member_benefits_form
						})
					},
					renderForm: function(params) {
						var that = this;

						var data = params.data;

						var init = function() {
							that.processForm({
								theForm: data.find('.commentForm')
							})
						}

						init();
					},
					processForm: function (params){

						var defaults = {
							rules: {
								email: {
									required: true,
									email: true
								},
								office_phone:{
									required: false,
									phoneUS: true
								},
								cell_phone:{
									required: false,
									phoneUS: true
								},
								subscribe_email:{
									required: true,
									email: true
								}
							},
							messages: {
								email: {
									email: "Your email address must be in the format of name@domain.com"
								},
								telephone: {
									phoneUS: "Your phone number must be in the format of 212-555-1000"
								},
							},
							errorPlacement: function(error, element) {
								switch(element.attr("name")){
									case "spam":
									$("input[name='spam']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
									break;
									case "transportation":
									$("input[name='transportation']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
									break;
									case "subscribe_email":
									element.parents('fieldset').after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
									break;
									default:
									element.parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
								}
							},
							invalidHandler: function(event, validator) {
								errors = validator.numberOfInvalids();
								if (errors) {
									message = errors == 1 ? 'You missed 1 field.' : 'You missed ' + errors + ' fields.';
									$(".formResponse").html(message);
									$(".formResponse").show();
								} else {
									$(".formResponse").hide();
								}
							},
							submitHandler: function(thisForm) {
								formData = theForm.serialize();
								postArgs = {
									challenge: $('#recaptcha_challenge_field').val(),
									response: $('#recaptcha_response_field').val()				
								}

								if(doCaptcha){
									$.post(pageDir + "/machines/libraries/recaptcha/recaptchaResponse.php", postArgs, function(data){
										if(data.substring(0,4) == "true"){
											sendForm();
										} else {
											Recaptcha.reload()
											returnObject = $('<div class="fieldResponse">Incorrect Captcha, please try again</div>');
											$('#contactCaptcha').append(returnObject)
										}
									})
								} else {
									sendForm();
								}

							},
							errorClass: "formError",
							sendForm: function(){
								$.post(that.pageDir + php_url, { formData: formData, csvUpdate: csvUpdate }, function(data) {
									callback(data);
									if(data == "success"){
										theForm.find(".formResponse").html("Form sent!");
										theForm.find(".formResponse").css('display', 'block !important');
									} else {
										theForm.find(".formResponse").html("Form not sent. Please refresh the page and try again");
										theForm.find(".formResponse").css('display', 'block !important');
									}
								});
							},
							recaptcha_key: "6LcgYfASAAAAAAvTvfb5r97NKTNnMbnCUnVN2-bp",
							php_url: "/machines/handlers/processForm.php"
						}

						var that = this;
						var theForm = (typeof params.theForm !== "undefined") ? params.theForm : false;
						var doCaptcha = (typeof params.doCaptcha !== "undefined") ? params.doCaptcha : false;
						var callback = (typeof params.callback !== "undefined") ? params.callback : function(){};
						var php_url = (typeof params.php_url !== "undefined") ? params.php_url : defaults.php_url;
						var rules = (typeof params.rules !== "undefined") ? params.rules : defaults.rules;
						var messages = (typeof params.messages !== "undefined") ? params.messages : defaults.messages;
						var errorPlacement = (typeof params.errorPlacement !== "undefined") ? params.errorPlacement : defaults.errorPlacement;
						var invalidHandler = (typeof params.invalidHandler !== "undefined") ? params.invalidHandler : defaults.invalidHandler;
						var errorClass = (typeof params.errorClass !== "undefined") ? params.errorClass : defaults.errorClass;
						var sendForm = (typeof params.sendForm !== "undefined") ? params.sendForm : defaults.sendForm;
						var submitHandler = (typeof params.submitHandler !== "undefined") ? params.submitHandler : defaults.submitHandler;
						var recaptcha_key = (typeof params.recaptcha_key !== "undefined") ? params.recaptcha_key : defaults.recaptcha_key;

						var csvUpdate = (theForm.data('csvupdate') != "") ? theForm.data('csvupdate') : 'formData';

						if(doCaptcha){
							Recaptcha.create(recaptcha_key, theForm.find('.captchaField').attr('id'), {
								tabindex: 1,
								theme: "clean",
								callback: Recaptcha.focus_response_field
							});
						}	

						theForm.validate({
							errorClass: errorClass,
							submitHandler: submitHandler,
							invalidHandler: invalidHandler,
							errorPlacement: errorPlacement,
							messages: messages,
							rules: rules
						});

					},
					renderNewsItems: function(params){
						
						var that = this;

						var debugThis = false;

						var views = {
							news_item: that.php_news_item,
						}

						var filter = {
							news_items_by_type: function(){
								var returnListing = [];
								var news_item_type_id = (typeof that.accessor_slug_listNewsItemTypes[news_item_type_slug] !== 'undefined') 
									? that.accessor_slug_listNewsItemTypes[news_item_type_slug].post_id 
									: null;

								_.each(data['listNewsItems'], function(value_listNewsItems, index_listNewsItems){
									if(
										value_listNewsItems.ni_type == news_item_type_id
									){
										returnListing.push(value_listNewsItems)
									}
								})
								returnListing.sort(function(a,b){
									var aDate = parseInt(a.ni_date)
									var bDate = parseInt(b.ni_date)
									return bDate - aDate
								})
								return returnListing;
							}
						}

						var render = {
							no_image: function(news_items){
								var news_item_container = render.listing(params);
								_.each(news_items, function(value_news_items, index_news_items){
									var newsItem = that.renderModel({
										ni_date: moment(value_news_items.ni_date, 'X').format('MMMM D, YYYY'),
										the_title: value_news_items.the_title,
										ni_source: value_news_items.ni_source,
									}, $(views.news_item));
									newsItem.remove('.image_container');
									news_item_container.append(newsItem);
									newsItem.find('.ni_source').attr({
										'href': that.externalURL(value_news_items.ni_url),
										'target': '_blank'
									})
								});
							},
							image: function(news_items){
								var news_item_container = render.listing(params);
								_.each(news_items, function(value_news_items, index_news_items){
									var newsItem = that.renderModel({
										ni_date: moment(value_news_items.ni_date, 'X').format('MMMM D, YYYY'),
										the_title: value_news_items.the_title,
										ni_source: value_news_items.ni_source,
									}, $(views.news_item));
									news_item_container.append(newsItem);
									newsItem.find('.ni_source').attr({
										'href': that.externalURL(value_news_items.ni_url),
										'target': '_blank'
									})
									if(value_news_items.featuredImage != null){
										newsItem.find('.image_container').bsSlider(value_news_items.featuredImage)
									}
								});
							},
							listing: function(data){
								var news_item_container = $('<div class="news_item_container"></div>');
								news_item_container.addClass(mode);
								target_element.append(news_item_container);
								return news_item_container;
							}
						}

						var get = {
							data: function(callback){
								that.get.data.call(that, ['listNewsItems', 'listNewsItemTypes'], function(fetchedData){
									data = fetchedData;
									callback();
								});
							}
						}						

						var init = function(){
							get.data(function(){
								render[mode](filter.news_items_by_type())
							})
						}

						var target_element = (typeof params.target_element === 'undefined') ? null : params.target_element;
						var mode = (typeof params.mode === 'undefined' || typeof render[params.mode] === 'undefined') ? null : params.mode;
						
						var news_item_type_slug = (typeof params.news_item_type_slug === 'undefined') ? null : params.news_item_type_slug;
						
						var data;

						if(
							target_element != null
							&& mode != null
							&& news_item_type_slug != null
						){
							init();
						}
					}, // end renderNewsItem
					renderEventsCarousel: function (params) {
						var that = this;
						var events_carousel = $('<div class="events_carousel carousel_container"></div>');
						params.target_element.append(events_carousel)

						that.get.data.call(that, ['listEvents'], function(fetchedData){
							that.renderItems({
								target_element: events_carousel,
								elements_to_render: _.filter(fetchedData.listEvents, function(data) {
									return data.is_featured === 'Y';
								}),
								renderFunction: function(data, view){
									console.log( 'inside loop events')
									console.log( data )
									var eventItem = that.renderModel(data, $(that.php_event_item));
									view.append(eventItem)
									if(data.event_start != '' && data.event_end != ''){
										eventItem.find('.event_date_info .event_day').html(moment(data.event_start, 'X').format('MMMM D, YYYY'))
										eventItem.find('.event_date_info .event_time').html(moment(data.event_start, 'X').format('LT') + ' - ' + moment(data.event_end, 'X').format('LT'))										
									}
									if(data.event_url != ''){
										eventItem.find('.rsvp a').attr({
											'target': '_blank',
											'href': that.externalURL(data.event_url)
										})
										eventItem.find('.rsvp').removeClass('displayNone');
									}
								}
							});
							events_carousel.slick({
								infinite: true,
								slidesToShow: 1,
								slidesToScroll: 1,
								dots: true,
								adaptiveHeight: true,
								prevArrow: $(that.php_slick_arrow).addClass('prevArrow')[0].outerHTML,
								nextArrow: $(that.php_slick_arrow).addClass('nextArrow')[0].outerHTML
							});
						});
					},
					renderPastEventsCarousel: function(params){
						var that = this;
						var events_carousel = $('<div class="events_carousel carousel_container"></div>');
						params.target_element.append(events_carousel);

						var today = new Date();
						var dd = today.getDate();
						var mm = today.getMonth() + 1;
						var yyyy = today.getFullYear();

						that.get.data.call(that, ['listEvents'], function(fetchedData){

							var renderData = _.filter(fetchedData.listEvents, function(data) {
								var date_array = moment(data.event_start, 'X').format('MM DD YYYY, h:mm:ss a').split(' ');
								
								var month = parseInt(date_array[0]);
								var day = parseInt(date_array[1]);
								var year = parseInt(date_array[2]);
								var isValid = false;

					
								if (moment(data.event_start, 'X').isBefore(moment())) {
									isValid = true
								}
								
								// if(year < yyyy){
								// 	isValid = true;
								// } else if(year == yyyy && month < mm){
								// 	isValid = true;
								// } else if(year == yyyy && month == mm && day < dd){
								// 	isValid = true;
								// }

								if (data.events_other_checkbox_single === 'Y') {
									isValid = false;
								}				
								return isValid;
							})

							renderData.sort(function(a,b){
								var aDate = parseInt(a.event_start)
								var bDate = parseInt(b.event_start)
								return bDate - aDate
							})

							that.renderItems({
								target_element: events_carousel,
								elements_to_render: renderData,
								renderFunction: function(data, view){
									var eventItem = that.renderModel(data, $(that.php_past_event_item));
									view.append(eventItem)
									// eventItem.find('.image_container').bsSlider(data.featuredImage)

									that.get.images_data.call(that, [data.events_cb_featured_images], function(data){
										eventItem.find('.image').attr('src', data[0])
									});
									if(data.event_start != '' && data.event_end != ''){
										eventItem.find('.event_date_info .event_day').html(moment(data.event_start, 'X').format('MMMM D, YYYY'))
										eventItem.find('.event_date_info .event_time').html(moment(data.event_start, 'X').format('LT') + ' - ' + moment(data.event_end, 'X').format('LT'))										
									}
									// if(data.event_url != ''){
									// 	eventItem.find('.rsvp a').attr({
									// 		'target': '_blank',
									// 		'href': that.externalURL(data.event_url)
									// 	})
									// 	eventItem.find('.rsvp').removeClass('displayNone');
									// }

									var page_id = 'events-test';
									var post_id = that.slugify(_.unescape(data.the_title));
									eventItem.find('.event_item_link').attr({
										'data-pageid': page_id,
										'data-postid': post_id,
										'href': '/' + page_id + '/' + post_id + '/'
									})
								}
							});
							// events_carousel.slick({
							// 	infinite: true,
							// 	slidesToShow: 2,
							// 	slidesToScroll: 1,
							// 	dots: false,
							// 	adaptiveHeight: true,
							// 	prevArrow: $(that.php_slick_arrow).addClass('prevArrow')[0].outerHTML,
							// 	nextArrow: $(that.php_slick_arrow).addClass('nextArrow')[0].outerHTML
							// });
						});
					},
					truncateWithEllipses: function(yourString, maxLength) {
						  // get the index of space after maxLength
						const index = yourString.indexOf(" ", maxLength);
						return index === -1 ? yourString : yourString.substring(0, index)
					},
					renderEventsCarousel_test: function(params){
						var that = this;
						var events_carousel = $('<div class="events_carousel carousel_container"></div>');
						params.target_element.append(events_carousel);

						var today = new Date();
						var dd = today.getDate();
						var mm = today.getMonth() + 1;
						var yyyy = today.getFullYear();

						that.get.data.call(that, ['listEvents'], function(fetchedData){

							var renderData = _.filter(fetchedData.listEvents, function(data) {
								var date_array = moment(data.event_start, 'X').format('MM DD YYYY').split(' ');
								var month = parseInt(date_array[0]);
								var day = parseInt(date_array[1]);
								var year = parseInt(date_array[2]);
								var isValid = false;


								if(year > yyyy){
									isValid = true;
								} else if(year == yyyy && month > mm){
									isValid = true;
								} else if(year == yyyy && month == mm && day > dd){
									isValid = true;
								} 

								if (data.events_other_checkbox_single === 'Y') {
									isValid = false;
								}
								
								return isValid;
							})

							renderData.sort(function(a,b){
								var aDate = parseInt(a.event_start)
								var bDate = parseInt(b.event_start)
								return aDate - bDate
							})
							that.renderItems({
								target_element: events_carousel,
								elements_to_render: renderData,
								renderFunction: function(data, view){
									if(that.slugify(_.unescape(data.the_title)) != 'february-2019-breakfast'){

										var eventItem = that.renderModel(data, $(that.php_event_item));
										view.append(eventItem)

										//Add background image to container 
										$(eventItem).find('.image_container').css('background-image', 'url(' + data.featuredImage + ')');

										if(data.event_tbd_date != ''){
											eventItem.find('.event_date_info').html(_.unescape(data.event_tbd_date))
										} else if (data.event_start != '' && data.event_end != ''){
											eventItem.find('.event_date_info .event_day').html(moment(data.event_start, 'X').format('MMMM D, YYYY'))
											eventItem.find('.event_date_info .event_time').html(moment(data.event_start, 'X').format('LT') + ' - ' + moment(data.event_end, 'X').format('LT'))
										}
										if(data.event_url != ''){
											eventItem.find('.register_button a').attr({
												'target': '_blank',
												'href': that.externalURL(data.event_url)
											})
											eventItem.find('.register_button').removeClass('displayNone');
										}

										var page_id = 'events-test';
										var post_id = that.slugify(_.unescape(data.the_title));
										if(data.event_url != ''){
											eventItem.find('a.event_item_link').attr({
											'data-pageid': page_id,
											'data-postid': post_id,
											'href': '/' + page_id + '/' + post_id + '/'
											})
											eventItem.find('.learn_more').removeClass('displayNone');
										}

	
									}

								}
							});
							// events_carousel.slick({
							// 	infinite: true,
							// 	slidesToShow: 4,
							// 	slidesToScroll: 1,
							// 	dots: true,
							// 	adaptiveHeight: true,
							// 	prevArrow: $(that.php_slick_arrow).addClass('prevArrow')[0].outerHTML,
							// 	nextArrow: $(that.php_slick_arrow).addClass('nextArrow')[0].outerHTML
							// });
						});
					},
					renderSponsorsCarousel: function (params) {
						var that = this;
						var type = (typeof params.type === 'undefined') ? '' : params.type;
						var sponsors_carousel = $('<div class="sponsors_carousel carousel_container"></div>');
						params.target_element.append(sponsors_carousel)

						that.get.data.call(that, ['listSponsors', 'listSponsorTypes'], function(fetchedData){
							that.renderItems({
								target_element: sponsors_carousel,
								elements_to_render: _.filter(that.returnAndSaveJsonData('listSponsors'), function (value_listSponsors, index_listSponsors) {
									return value_listSponsors.sponsor_type == that.accessor_slug_listSponsorTypes[type].post_id
								}),
								renderFunction: function(data, view){
									var sponsor_item = $(that.php_sponsor_item);
									view.append(sponsor_item)
									sponsor_item.find('.image_block').bsSlider(data.featuredImage)
									sponsor_item.find('a').attr({
										'target': '_blank',
										'href': that.externalURL(data.client_url)
									})
								}
							});
							sponsors_carousel.slick({
								infinite: true,
								slidesToShow: 5,
								slidesToScroll: 5,
								dots: true,
								autoplay: true,
								prevArrow: $(that.php_slick_arrow).addClass('prevArrow')[0].outerHTML,
								nextArrow: $(that.php_slick_arrow).addClass('nextArrow')[0].outerHTML,
								responsive: [
									{
										breakpoint: 1199,
										settings: {
											slidesToShow: 3,
											slidesToScroll: 3,
											dots: false,
										}
									},
									{
										breakpoint: 768,
										settings: {
											slidesToShow: 2,
											slidesToScroll: 2,
											dots: false,
										}
									},
									{
										breakpoint: 479,
										settings: {
											slidesToShow: 1,
											slidesToScroll: 1,
											dots: false,
											autoplay: true
										}
									},
								]
							});
						});
					},
					renderTestimonialsCarousel: function (params) {
						var that = this;
						var testimonial_container = $('<div class="testimonial_container"></div>');
						params.target_element.append(testimonial_container)

						that.get.data.call(that, ['listTestimonials'], function(fetchedData){
							that.renderItems({
								target_element: testimonial_container,
								elements_to_render: fetchedData.listTestimonials,
								renderFunction: function(data, view){
									var returnObject = that.renderModel(data, $(that.php_testimonial_item));
									view.append(returnObject);
									returnObject.find('.image_block').bsSlider(data.featuredImage);
								}
							});
							testimonial_container.slick({
								infinite: true,
								speed: 400,
								autoplay: true,
								autoplaySpeed: 4000,
								slidesToShow: 1,
								slidesToScroll: 1,
								prevArrow: $(that.php_slick_arrow).addClass('prevArrow')[0].outerHTML,
								nextArrow: $(that.php_slick_arrow).addClass('nextArrow')[0].outerHTML
							});
						});
					},
					startUp: function(){
				    	var that = this;
				    	that.setPageID(that.RewriteBase);

						if(window['pageID'] == "admin"){
							that.loadDatePicker($('.date'));
							that.loadSortable($(".sortable"));
							if($('#people_sample_hidden_meta').size() != 0){
								$('#people_sample_hidden_meta').find('.hidden_meta').val('I was generated dynamically')
							}
							$('head').append('<link rel="stylesheet" id="jquery-style-css" href="' + that.pageDir + '/styles/admin-styles.min.css" type="text/css" media="all">');
							that.adminPages(pagenow);
						} else {
							that.setPopstate();
							that.fixLinks();
							that.loadJqueryHelpers({ 
								'bsSlider': that.bsSlider 
							});

							if(typeof pageID !== "undefined"){
								var testimonialsCarousel = $('<div class="testimonialsCarousel"></div>')
								$('.pageSection').after(testimonialsCarousel)
								that.renderTestimonialsCarousel({
									target_element: testimonialsCarousel
								})
								that.loadView(pageID, postID);
								that.startUpRan = true;
							}
						}
				    },
				    fixLinks: function(params){
				    	var that = this;
				    	$('.navbar, .footer').find('a').each(function(){
				    		var isInternalLink = ($(this).hasClass('hyperlink')) ? false : true;
				    		var isNotEmail =  ($(this).attr('href').indexOf('mailto:') == -1) ? true : false;
				    		var isLogo = ($(this).hasClass('navbar-brand')) ? true : false;
				    		var isNotDynamicMenu = ($(this).hasClass('dynamic-menu-item')) ? false : true;
				    		var isNotCustom = ($(this).parents('.menu-item-type-custom').length == 0) ? true : false;

				    		if(isInternalLink && isNotEmail && isNotDynamicMenu && isNotCustom){
				    			var pageSlug = (isLogo) ? that.defaultPage : that.slugify($(this).text().replace('&amp;', ''))
				    			var newURL = '/' + pageSlug + '/';
				    			$(this).attr('href', newURL);
				    			$(this).addClass('menu-item-' + pageSlug);
				    			$(this).attr('data-pageid', pageSlug);

				    			$(this).off('click');
				    			$(this).on('click', function(e){
				    				e.preventDefault();
				    				that.pushPageNav($(this).attr('data-pageid'))
				    			})
				    		}
				    		if(!isNotCustom){
				    			$(this).parents('.menu-item-type-custom').on('click', function(e){
				    				e.preventDefault();
				    				window.open($(this).find('a')[0].href, '_blank');
				    			})
				    		}
				    	});
				    },
					renderSlideshow: function(params){

						// // USAGE
						// 	that.renderSlideshow({
						// 		target_element: $('body'),
						// 		slides_data: [
						// 			{
						// 				image_url: 'http://via.placeholder.com/350x150',
						// 				image_dom: 'test123'
						// 			}
						// 		],
						// 		views: {
						// 			text_container: '<div class="container"><div class="text_container"></div></div>',
						// 		}
						// 	});

						var default_views = {
							slideshow_container: '<div class="slideshow_container"></div>',
							image_container: '<div class="image_container"></div>',
							text_container: '<div class="text_container"></div>'
						};

						var that = this;

						var target_element = (typeof params.target_element === "undefined") ? null : params.target_element;
						var slides_data = (typeof params.slides_data === "undefined") ? null : params.slides_data;
						var views = (typeof params.views === "undefined") ? default_views : params.views;
						var on_image_change = (typeof params.on_image_change === "undefined") ? function(){} : params.on_image_change;
						views.slideshow_container = (typeof views.slideshow_container === "undefined") ? default_views.slideshow_container : views.slideshow_container;
						views.image_container = (typeof views.image_container === "undefined") ? default_views.image_container : views.image_container;
						views.text_container = (typeof views.text_container === "undefined") ? default_views.text_container : views.text_container;

						var render = {
							slideshow_container: function (argument) {
								var returnObject = $(views.slideshow_container);
								returnObject.append(views.image_container);
								returnObject.append(views.text_container);
								return returnObject;
							},
							slide: function(imageUrl){
								target_element.append(slideshow_container);
								slideshow_container.find('.image_container').bsSlider(imageUrl);
							}
						}

						var init = function(){
							// build slideshow DOM
							var slideshow_object = render.slideshow_container();

							// build image array
							var image_array = _.map(slides_data, function(value_slides_data, index_slides_data){
								return value_slides_data.image_url;
							});

							// add slider to target_element
							target_element.html(slideshow_object);

							// run slider
							target_element.bsSlider(image_array, {
								on_image_change: function (index) {
									if(typeof slides_data[index] !== 'undefined'){
										slideshow_object.find('.text_container').html(_.unescape(slides_data[index].image_dom))
										slideshow_object.find('.si_editor_2').html(_.unescape(slides_data[index].image_secondary_editor))
										on_image_change(index, slides_data[index])
									}
								}
							});
						}

						if(target_element != null && slides_data != null){
							init();
						}
					},
					renderBlockquoteSlideshow: function(params){
						var that = this;

						var element = (typeof params.blockquote_parent === 'undefined') ? null : params.blockquote_parent;
						var secondary_color = (params.secondary_color === '') ? '' : params.secondary_color;

						if(element.find('blockquote').length > 0){
							element.find('blockquote').each(function(){
								if($(this).children('ul').find('li > img').length > 0){
									var imageUrls = []
									$(this).children('ul').find('li > img').each(function(){
										imageUrls.push({
											image_url: $(this).attr('src'),
											image_dom: ''
										});
									});
									var blockquote_slideshow = $(that.php_blockquote_slideshow);
									$(this).children('ul').after(blockquote_slideshow)
									$(this).children('ul').remove()

									if(imageUrls.length > 1){
										_.each(imageUrls, function (argument, index) {
											var slideButton = $('<div class="slideButton"></div>')
											slideButton.attr({
												'data_index': index
											})
											blockquote_slideshow.find('.blockquoteSlideshowControls').append(slideButton)
										})
										blockquote_slideshow.find('.slide_index').html('1/' + imageUrls.length);
									}
									blockquote_slideshow.find('.blockquoteSlideshowBgBox').css('background-color', secondary_color);

									that.renderSlideshow({
										target_element: blockquote_slideshow.find('.blockquoteSlideshow'),
										slides_data: imageUrls,
										on_image_change: function (index, data) {
											blockquote_slideshow.find('.slideButton').removeClass('active')
											blockquote_slideshow.find('.slideButton').eq(index).addClass('active')
											if(imageUrls.length > 1){
												blockquote_slideshow.find('.slide_index').html((index + 1) + '/' + imageUrls.length)
											}
										}
									});

									if(imageUrls.length > 1){
										blockquote_slideshow.find('.slideButton').on('click', function (argument) {
											var this_index = $(this).attr('data_index');
											blockquote_slideshow.find('.blockquoteSlideshow').data('bs_slider').go_to_slide(this_index)
											blockquote_slideshow.find('.slide_index').html((++this_index) + '/' + imageUrls.length)
										})
									}

								} else {
									$(this).css('background-color', secondary_color);
									$(this).addClass('special_blockquote');
								}
							})
						}

					},
					renderPeople: function(params){
						var that = this;

						var debugThis = false;

						var views = {
							person_item: that.php_person_item,
							person_item_bio: that.php_person_item_bio
						}

						var filter = {
							people_by_type: function(){
								var returnListing = [];
								var people_type_id = (typeof that.accessor_slug_listPeopleTypes[people_type_slug] !== 'undefined') ? that.accessor_slug_listPeopleTypes[people_type_slug].post_id : null;

								_.each(data['listPeople'], function(value_listPeople, index_listPeople){
									if(
										value_listPeople.person_type == people_type_id
										&& value_listPeople.person_first_name !== ''
										&& value_listPeople.person_last_name !== ''
										&& value_listPeople.featuredImage !== null
									){
										returnListing.push(value_listPeople)
									}
								})

								return returnListing;
							}
						}

						var render = {
							person_item: function(params, useExternalUrl){
								useExternalUrl = (typeof useExternalUrl === 'undefined') ? false : useExternalUrl;
								var person_data = params.person_data;
								var target = params.target;
								var person_object = that.renderModel(person_data, $(views.person_item));
								var data_pageid = 'leadership';
								var data_postid = that.slugify(person_data.person_first_name + ' ' + person_data.person_last_name);
								var href = '/' + data_pageid + '/' + data_postid + '/';
								target.append(person_object)
								person_object.attr('data-personid', data_postid);								
								person_object.find('.person_image').bsSlider(person_data.featuredImage)
								if(useExternalUrl){
									if(person_data.person_bio_url != ''){
										person_object.find('a').attr({
											'href': that.externalURL(person_data.person_bio_url),
											'target': '_blank'
										})
									}
								} else {
									person_object.find('a').attr({
										'href': href,
										'data-pageid': data_pageid,
										'data-postid': data_postid,
									})
									person_object.find('a').off('click');
									person_object.find('a').on('click', function(e){
										e.preventDefault();
										that.pushPageNav($(this).attr('data-pageid'), $(this).attr('data-postid'))
									});
								}
							},
							listing: function(data, useExternalUrl){
								useExternalUrl = (typeof useExternalUrl === 'undefined') ? false : useExternalUrl;
								var person_container = $('<div class="person_container"></div>');
								person_container.addClass(mode);
								target_element.append(person_container)
								_.each(data, function(value_data, index_data){
									render.person_item({
										person_data: value_data,
										target: person_container
									}, useExternalUrl)
								})
							},
							carousel: function(data){
								render.listing(data, true);
								target_element.find('.person_container').addClass('carousel_container');
								target_element.find('.person_container').slick({
									infinite: true,
									slidesToShow: 5,
									slidesToScroll: 5,
									dots: true,
									prevArrow: $(that.php_slick_arrow).addClass('prevArrow')[0].outerHTML,
									nextArrow: $(that.php_slick_arrow).addClass('nextArrow')[0].outerHTML,
									responsive: [
										{
											breakpoint: 1199,
											settings: {
												slidesToShow: 3,
												slidesToScroll: 3,
												dots: false,
											}
										},
										{
											breakpoint: 768,
											settings: {
												slidesToShow: 2,
												slidesToScroll: 2,
												dots: false,
											}
										},
										{
											breakpoint: 479,
											settings: {
												slidesToShow: 1,
												slidesToScroll: 1,
												dots: false,
												autoplay: true
											}
										},
									]
								});
							},
							listing_bio: function(data){
								render.listing(data, false);
								target_element.find('.person_container').wrap('<div class="container"></div>');
								var person_bio_container;
								if(target_element.find('.person_bio_container').length > 0){
									target_element.find('.person_bio_container').html('')
									person_bio_container = target_element.find('.person_bio_container');
								} else {
									person_bio_container = $('<div class="person_bio_container"></div>');
									target_element.append(person_bio_container)
								}

								if(!that.postIDnotSet() || debugThis){
									var thisPerson;
									var thisPostID = (debugThis == true) ? 'jane-doe' : postID;
									target_element.find('.person_container').find('.person_item.active').removeClass('active');
									target_element.find('.person_container').find('.person_item[data-personid="' + postID + '"]').addClass('active');
									_.each(webApp.accessor_listPeople, function (value_listPeople, index_listPeople) {
										if(thisPostID === that.slugify(value_listPeople.person_first_name + ' ' + value_listPeople.person_last_name)){
											thisPerson = value_listPeople;
										}
									})
									person_bio_container.html(that.renderModel(thisPerson, $(views.person_item_bio)))
								}
							}
						}

						var get = {
							data: function(callback){
								that.get.data.call(that, ['listPeople', 'listPeopleTypes'], function(fetchedData){
									data = fetchedData;
									callback();
								});
							}
						}						

						var init = function(){
							get.data(function(){
								render[mode](filter.people_by_type())
							})
						}

						var target_element = (typeof params.target_element === 'undefined') ? null : params.target_element;
						var mode = (typeof params.mode === 'undefined' || typeof render[params.mode] === 'undefined') ? null : params.mode;
						var people_type_slug = (typeof params.people_type_slug === 'undefined') ? null : params.people_type_slug;
						var data;

						if(
							target_element != null
							&& mode != null
							&& people_type_slug != null
						){
							init();
						}

					},
					renderSpeakers: function (data, view) {
						var that = this;
						that.renderPeople({
							target_element: view.find('.extra_dom'),
							mode: 'carousel',
							people_type_slug: 'speaker'
						})
					},
					renderMapContact: function(data, view) {
						var that = this;

						var contact_map = $(that.php_contact_map);
						view.find('.extra_dom').append(contact_map)

						// that.renderMap({
						// 	targetElement: contact_map.find('.container_map'),
						// 	locationsArray: [{
						// 		the_title: 'NYC Builds Bio+',
						// 		location_latitude: '40.750807',
						// 		location_longitude: '-73.976559'
						// 	}]
						// });
					},
					renderMap: function(params) {
						var that = this;
						var targetElement = (typeof params.targetElement === 'undefined') ? $('body') : params.targetElement;
						var locationsArray = (typeof params.locationsArray === 'undefined') ? [] : params.locationsArray;

						var views = {
							map: that.php_map
						}

						var render = {
							map: function(){

								var all_locations_array = build.locations_array(locationsArray);
								var all_locations_array_no_title = build.locations_array(locationsArray, 'no_title');
								var cities_group_array = build.cities_group(all_locations_array);
								var cities = L.layerGroup(cities_group_array);
								// var mapboxUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
								var mapboxUrl = 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png';
								var mapboxAttribution = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
								var grayscale = L.tileLayer(mapboxUrl, {id: 'mapid', attribution: mapboxAttribution});

								var map = build.map({
									map_id: 'mapid',
									grayscale: grayscale,
									cities: cities
								});

								map.scrollWheelZoom.disable();
								map.removeControl(map.zoomControl);
								map.fitBounds(all_locations_array_no_title, {padding: [30, 30]})

							},
							map_view: function(){
								targetElement.append(views.map)
							}
						}

						var build = {
							locations_array: function(location_data, mode){
								var locations_array = [];

								switch(mode){
									case 'no_title':
									_.each(location_data, function(value_location_data, index_location_data){
										var location_object = [];

										location_object.push(value_location_data.location_latitude, value_location_data.location_longitude);
										locations_array.push(location_object);
									})
									break;

									default:
									_.each(location_data, function(value_location_data, index_location_data){
										var location_object = [];

										location_object.push(value_location_data.the_title, value_location_data.location_latitude, value_location_data.location_longitude);
										locations_array.push(location_object);
									})
									break;
								}

								return locations_array;
							},
							cities_group: function(locations_array){
								var cities_group = []

								_.each(locations_array, function(value_locations_array, index_locations_array){
									var city_locations_group = L.marker([value_locations_array[1], value_locations_array[2]]).bindPopup(value_locations_array[0]);
									cities_group.push(city_locations_group)

								})

								return cities_group;
							},
							map: function(params){
								return L.map(params.map_id, {
									layers: [params.grayscale, params.cities],
									attributionControl: false
								});
							}
						}

						var init = function(){
							render.map_view();
							render.map();
						}

						init();
					},
					renderSteeringCommittee: function (data, view) {
						var that = this;
						that.renderPeople({
							target_element: view.find('.extra_dom'),
							mode: 'listing',
							people_type_slug: 'steering-committee'
						})
					},
					renderSteeringCommitteeBio: function (data, view) {
						var that = this;
						that.renderPeople({
							target_element: view.find('.extra_dom'),
							mode: 'listing_bio',
							people_type_slug: 'steering-committee'
						})
					},
					renderSponsors: function (data, view) {
						var that = this;
						var sponsorsCarousel = $('<div class="sponsorsCarousel"></div>');
						view.find('.extra_dom').append(sponsorsCarousel);
						that.renderSponsorsCarousel({
							target_element: sponsorsCarousel,
							type: 'event-sponsors'
						});
					},
					renderFoundingSponsors: function (data, view) {
						var that = this;
						var foundingSponsorsCarousel = $('<div class="foundingSponsorsCarousel"></div>');
						view.find('.extra_dom').append(foundingSponsorsCarousel);
						that.renderSponsorsCarousel({
							target_element: foundingSponsorsCarousel,
							type: 'founding-members'
						});
					},
					renderNewsListing: function (data, view) {
						
						var that = this;
						that.renderNewsItems({
							target_element: view.find('.extra_dom'),
							mode: 'image',
							news_item_type_slug: 'news'
						});
					},
					// added 09-05-19
					// duplicating renderNewsListing to add renderNewsletterListing
					// this will allow the newsletter to be rendered like the news
					renderNewsletterListing: function (data, view) {
						
						var that = this;
						that.renderNewsItems({
							target_element: view.find('.extra_dom'),
							mode: 'image',
							news_item_type_slug: 'newsletter'
						});
					},
					renderPressReleasesListing: function (data, view) {
						var that = this;
						that.renderNewsItems({
							target_element: view.find('.extra_dom'),
							mode: 'image',
							news_item_type_slug: 'press-release'
						});
					},
					renderBlogListing: function (data, view) {
						var that = this;
						that.renderNewsItems({
							target_element: view.find('.extra_dom'),
							mode: 'image',
							news_item_type_slug: 'blog'
						});
					},
					renderCalendar: function (data, view) {

						var that = this;

						var target_element = view.find('.extra_dom');

						var views = {
							event_calendar_item: that.php_event_calendar_item,
							month_container: that.php_month_container
						};

						var data = {};

						var get = {
							data: function(callback) {
								that.get.data.call(that, ['listEvents'], function(fetchedData) {
									data['listEvents'] = _.sortBy(fetchedData.listEvents, 'event_start');
									callback();
								});
							}
						};

						var render = {
							thisMonthContainer: function (thisMonthYear, value_listEvents) {
								var thisMonthContainer = $(views.month_container);
								thisMonthContainer.addClass(thisMonthYear);
								thisMonthContainer.find('.month_title_container').html(moment(value_listEvents.event_start, 'X').format('MMMM YYYY'))
								return thisMonthContainer;
							},
							monthYearObject: function () {
								var container = $('<div class="container"></div>');
								var monthYearObject = $('<ul class="monthYearObject"></ul>');
								target_element.append(container);
								container.append(monthYearObject);
								return monthYearObject;
							},
							thisEventCalendarItem: function (value_listEvents) {
								var thisEventCalendarItem = that.renderModel({
									event_day: moment(value_listEvents.event_start, 'X').format('DD'),
									event_day_of_week: moment(value_listEvents.event_start, 'X').format('ddd'),
									event_start: moment(value_listEvents.event_start, 'X').format('h:mm A'),
									event_end: moment(value_listEvents.event_end, 'X').format('h:mm A'),
									the_title: value_listEvents.the_title
								}, $(views.event_calendar_item));
								return thisEventCalendarItem;
							}
						};

						var bind = {
							carousel: function(monthYearObject){
								monthYearObject.slick({
									infinite: true,
									slidesToShow: 1,
									slidesToScroll: 1,
									arrows: false,
									adaptiveHeight: true
								});
							},
							carousel_arrows: function(monthYearObject){
								monthYearObject.find('.prev_button_container').off('click')
								monthYearObject.find('.prev_button_container').on('click', function(e){
									e.preventDefault();
									monthYearObject.slick('slickPrev');
								});

								monthYearObject.find('.next_button_container').off('click')
								monthYearObject.find('.next_button_container').on('click', function(e){
									e.preventDefault();
									monthYearObject.slick('slickNext');
								});
							}
						}

						var init = function() {

							get.data(function(){

								var monthYearObject = render.monthYearObject();

								_.each(data['listEvents'], function(value_listEvents, index_listEvents) {

									var thisMonthYear = moment(value_listEvents.event_start, 'X').format('MMMM-Y');
									var thisMonthContainer = monthYearObject.find('.' + thisMonthYear);

									if(thisMonthContainer.length === 0) {

										thisMonthContainer = render.thisMonthContainer(thisMonthYear, value_listEvents);
										monthYearObject.append(thisMonthContainer);

									}

									var thisEventCalendarItem = render.thisEventCalendarItem(value_listEvents);
									thisMonthContainer.find('ul').append(thisEventCalendarItem)

								});

								bind.carousel(monthYearObject);
								bind.carousel_arrows(monthYearObject);

							});
						}

						init();


					},
					renderTestimonials: function () {
						// console.log('renderTestimonials');
					},
					renderUpcomingEvents: function (data, view) {
						// var that = this;
						// var eventsCarousel = $('<div class="eventsCarousel"></div>');
						// view.find('.extra_dom').append(eventsCarousel);
						// that.renderEventsCarousel({
						// 	target_element: eventsCarousel
						// });

						var that = this;


						var eventsCarousel = $('<div class="eventsCarousel"></div>');
						view.find('.extra_dom').append(eventsCarousel);
						that.renderEventsCarousel_test({
							target_element: eventsCarousel
						});
					},
					makeBannerClickable: function (data, view) {
						var that = this;


						$('.slideshow_big').addClass('banner_image_container')
						$('banner_image_container').removeClass('slideshow_big')

						$('.banner_image_container').wrapAll('<a target="_blank" href="http://www.nycbuildsbio.org/events-test/life-sciences-2019-real-estate-development-symposium/" />')
						
					},
					renderUpcomingEventsBuildsBioNotAttending: function (data, view) {
						var that = this;
						var events_carousel = $('<div class="events_carousel carousel_container"></div>');
						view.append(events_carousel);

						var shouldHide = false;
						
						
						that.get.data.call(that, ['listEvents'], function(fetchedData){
							that.renderItems({
								target_element: events_carousel,
								elements_to_render: _.filter(fetchedData.listEvents, function(data) {
									var otherEvent = data.events_other_checkbox_single
									var isValid = false;

									if(otherEvent === 'Y'){
										isValid = true;
									} else if (otherEvent === '') {
										shouldHide = true;
									}
									
									return isValid;
								}),
								renderFunction: function(data, view){

									if(that.slugify(_.unescape(data.the_title)) != 'february-2019-breakfast'){
										var eventItem = that.renderModel(data, $(that.php_event_item));
										view.append(eventItem)

										if(data.event_tbd_date != ''){
											eventItem.find('.event_date_info').html(_.unescape(data.event_tbd_date))
										} else if (data.event_start != '' && data.event_end != ''){
											eventItem.find('.event_date_info .event_day').html(moment(data.event_start, 'X').format('MMMM D, YYYY'))
											eventItem.find('.event_date_info .event_time').html(moment(data.event_start, 'X').format('LT') + ' - ' + moment(data.event_end, 'X').format('LT'))
										}
										if(data.event_url != ''){
											eventItem.find('.rsvp a').attr({
												'href': that.externalURL(data.event_url)
											})
											eventItem.find('.rsvp').removeClass('displayNone');
											eventItem.find('.learn_more').addClass('displayNone');
										}
									}

								}
							});
						});

						if (shouldHide) {
							$('.events-nyc-builds-bio-will-be-attending').addClass("displayNone")
						}


					},
					renderPastEvents: function(data, view){

						var that = this;
						var eventsCarousel = $('<div class="eventsCarousel"></div>');
						view.find('.extra_dom').append(eventsCarousel);
						that.renderPastEventsCarousel({
							target_element: eventsCarousel
						});
					},
					renderContentBox: function (data, view) {
						var that = this;
						// console.log('renderContentBox', data, view);
					},
					getCBviews: function(){
						return {
							callout_box_title_bg: this.php_callout_box_title_bg,
							callout_box_title_side: this.php_callout_box_title_side,
							text_1_col_title_side: this.php_text_1_col_title_side,
							text_1_col_title_top: this.php_text_1_col_title_top,
							text_1_col_title_bg: this.php_text_1_col_title_bg,
							text_2_col_title_side: this.php_text_2_col_title_side,
							text_2_col_no_title: this.php_text_2_col_no_title,
							text_2_col_title_side_66_33: this.php_text_2_col_title_side_66_33,
							slideshow_big: this.php_slideshow_big,
							map: this.php_map,
							content_100: this.php_content_100
						};
					},
					renderContentBoxes: function (params) {
						var that = this;

						var default_view = '<div class="content_box"><div class="the_title"></div></div>';

						var target_element = (typeof params.target_element === "undefined") ? null : params.target_element;
						var current_page = (typeof params.current_page === "undefined") ? pageID : params.current_page;
						var on_element_render = (typeof params.on_element_render === "undefined") ? function(){} : params.on_element_render;
						var content_boxes_model = (typeof params.content_boxes_model === "undefined") ? 'listContentBoxes' : params.content_boxes_model;
						var pages_model_cb_field = (typeof params.pages_model_cb_field === "undefined") ? 'pages_content_boxes' : params.pages_model_cb_field;
						var cb_model_style_field = (typeof params.cb_model_style_field === "undefined") ? 'cb_layout' : params.cb_model_style_field;

						var views = (typeof params.views === "undefined") ? that.getCBviews() : params.views;

						var render = {
							content_box: function(cb_data, cb_view, done_function){
								var returnObject = that.renderModel(cb_data, $(cb_view));
								returnObject.find('.displayNone').each(function(){
									if($(this).html() != ''){
										$(this).removeClass('displayNone');
									}
								})
								target_element.append(returnObject);
								returnObject.addClass('content_box');
								returnObject.addClass(that.slugify(_.unescape(cb_data.the_title)));
								this.style.bgColor(returnObject, cb_data.cb_bg_color);

								if(cb_data.cb_layout.indexOf('callout_box') !== -1){
									this.style.bgColor(returnObject.find('.cb_editor_2'), cb_data.cb_bg_color_2);
								}

								if(cb_data.cb_layout.indexOf('map') !== -1){
									this.style.bgColor(returnObject.find('.mapBG'), cb_data.cb_bg_color_2);
								}

								this.button(returnObject, cb_data);

								that.render_user_defined_function({
									data: cb_data,
									view: returnObject
								});

								done_function(cb_data, returnObject);
								return returnObject;
							},
							button: function(cb_object, data){
								var cb_page_text = (typeof data.cb_page_text !== 'undefined' && data.cb_page_text != '') ? data.cb_page_text : null;
								var cb_page_link = (typeof data.cb_page_link !== 'undefined' && data.cb_page_link != '') ? data.cb_page_link : null;

								if(cb_page_text != null && cb_page_link != null){
									var this_button = that.renderButton({
										button_dom: cb_page_text,
										button_pageID: that.slugify(_.unescape(that.accessor_ids_listPages[cb_page_link].the_title)),
									});
									cb_object.find('.button_container').append(this_button);
								}

							},
							style: {
								bgColor: function(cb, bgColor){
									if(bgColor != ''){
										cb.css('background-color', bgColor)
									}
								}
							}
						}

						var init = function(){
							that.get.data.call(that, [content_boxes_model], function(data){
								var this_pages_content_boxes = that.accessor_listPages[current_page][pages_model_cb_field];
								var color_memory = null;
								_.each(this_pages_content_boxes, function(value_this_pages_content_boxes, index_this_pages_content_boxes){
									var cb_data = that['accessor_' + content_boxes_model][value_this_pages_content_boxes];
									var cb_view = (typeof views[cb_data[cb_model_style_field]] === 'undefined') ? default_view : views[cb_data[cb_model_style_field]];

									var this_content_box = render.content_box(cb_data, cb_view, on_element_render);

									if(cb_data.cb_bg_color == color_memory){
										this_content_box.addClass('duplicate_color');
										this_content_box.prev().addClass('duplicate_color_prev');
									}

									color_memory = cb_data.cb_bg_color;

									
								});
							});
						}

						if(content_boxes_model != null && pages_model_cb_field != null && target_element != null){
							init();
						}
					},
					renderSlideshowBig: function(data, element) {
						that = this;
						that.get.data.call(that, ['listSlideImages'], function(){
							that.renderSlideshow({
								target_element: element,
								slides_data: (function(){
									var imagesData = [];
									_.each(data.cb_slide_images, function(images){
										var thisImage = that.accessor_listSlideImages[images];
										if(thisImage.featuredImage != null){
											imagesData.push({
												image_url: thisImage.featuredImage,
												image_dom: thisImage.the_content,
												image_secondary_editor: thisImage.si_editor_2
											})
										}
									})
									return imagesData;
								}()),
								views: {
									text_container: '<div class="container"><div class="text_block"><div class="text_content"><div class="text_container"></div></div><div class="si_editor_2"></div></div></div>',
								}
							});
						});
					},
					cleanUp: function(pageIDrequest, postIDrequest){
						var that = this;

						// if IE 8
						if(!($('html').hasClass('internet-explorer-8') || js_mobile == "true")){
							that.bindMenuAnimations($('#menu-main-menu'));
						}

						// if mobile device
						if(js_mobile == "true"){
							if($('#mm-navbar').size() == 0){
								that.multiLevelPushMenu(jQuery('#navbar'))
							}
							$("#mm-navbar").trigger("close.mm");
						}

						// page specific cleanups
						switch(pageIDrequest){
							default:
							break;
						}
					},
					multiLevelPushMenu: function(target, position) {

						position = (typeof position === "undefined") ? 'right' : position;

						targetClone = target;
						targetClone.find('*').removeAttr('class')
						targetClone.removeAttr('class')

						targetClone.mmenu({
							autoHeight: true,
							offCanvas: {
								position: position
							},
							onClick: {
								close: function() {
									return ($(this).attr('data-pageid') != "");
								}
							}
						}, {
							clone: true
						});

						$('.mm-page').append($('.pageSection'))
						$('.mm-page').append($('.testimonialsCarousel'))
						$('.mm-page').append($('.footer'))

						$('.navbar-toggle').on('click', function(e){
							if($('#mm-navbar').hasClass('mm-opened')){
								$("#mm-navbar").trigger("close.mm");
							} else {
								$("#mm-navbar").trigger("open.mm");
							}
						});

					}

				}
			});
		}
	});

})(jQuery);