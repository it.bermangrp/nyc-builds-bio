<?php
	require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';

    // First check the nonce, if it fails the function will break
    // check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $status;
    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        $status = 'fail';
        // echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
    } else {
        $status = 'success';
        // echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
    }

    echo $status;

?>