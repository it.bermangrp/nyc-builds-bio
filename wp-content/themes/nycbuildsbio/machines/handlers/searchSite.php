<?php
	// starter functions
		require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';

		function recursive_array_search($needle,$haystack) {
			foreach($haystack as $key=>$value) {
				$current_key = $key;
				if(is_string($value)){
					if (strpos(strtolower($value), strtolower($needle)) !== false){
						$resO = true;
					} else {
						$resO = false;
					}
				} else {
					$resO = false;
				}
				if (strpos(strtolower($key), strtolower($needle)) !== false){
					if(strpos(strtolower($value), 'true') !== false){
						$resO = true;
					}
				}
				if($resO OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
					return true;
				}
			}
			return false;
		}

	// starter variables
		$searchArrayVal = $_POST['searchArray'];
		$returnArray = array();

	// search function
		foreach ($searchArrayVal as $key => $currentSearchTerm) {
			if($currentSearchTerm != ""){
				$allPageIDs = get_all_page_ids();
				foreach ($allPageIDs as $value) {
					// build result set
						$itemArray = array(
							'wp_page_id' => $value,
							'pageID' => sanitize_title(get_the_title($value)),
							'pageTitle' => get_the_title($value),
							'pageContent' => get_post_field('post_content', $value)
						);

					// search content
						$mystring = strtolower($itemArray['pageContent']);
						$findme   = strtolower($currentSearchTerm);
						$pos = strpos($mystring, $findme);
						if ($pos !== false) {
							$returnArray[] = $itemArray;
						} else {
						}

					// search title
						$mystring = strtolower($itemArray['pageTitle']);
						$findme   = strtolower($currentSearchTerm);
						$pos = strpos($mystring, $findme);
						if ($pos !== false) {
							$returnArray[] = $itemArray;
						} else {
						}

				}
			}
		}

	// remove duplicates
		function multi_array_unique($src){
			$output = array_map("unserialize", array_unique(array_map("serialize", $src)));
			return $output;
		}

		$returnArray = multi_array_unique($returnArray);


	// return data
		echo json_encode($returnArray, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

?>