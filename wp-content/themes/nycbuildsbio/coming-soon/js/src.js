(function(d) {
	var body_element = document.querySelectorAll('body')[0];
	var run_app = function (argument) {
		body_element.classList.remove('fonts_not_loaded');
	};
	var config = {
	        kitId: 'tsi2tlq',
	        scriptTimeout: 3000,
	        async: true
	    },
	    h = d.documentElement,
	    t = setTimeout(function() {
	        h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
	    }, config.scriptTimeout),
	    tk = d.createElement("script"),
	    f = false,
	    s = d.getElementsByTagName("script")[0],
	    a;
	h.className += " wf-loading";
	tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
	tk.async = true;
	tk.onload = tk.onreadystatechange = function() {
	    a = this.readyState;
	    if (f || a && a != "complete" && a != "loaded") return;
	    f = true;
		setTimeout(function(){
			run_app();
		}, 2000)
	    clearTimeout(t);
	    try {
	        Typekit.load(config)
	    } catch (e) {}
	};
	s.parentNode.insertBefore(tk, s)
})(document);