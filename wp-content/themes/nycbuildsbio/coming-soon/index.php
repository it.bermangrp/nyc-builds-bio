<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126195312-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-126195312-1');
	</script> -->

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>NYC Builds Bio</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="/wp-content/themes/nycbuildsbio/coming-soon/styles/styles.min.css">
	<link rel="icon" type="image/png" sizes="32x32" href="https://www.nycbuildsbio.org/wp-content/themes/nycbuildsbio/images/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="https://www.nycbuildsbio.org/wp-content/themes/nycbuildsbio/images/favicons/favicon-16x16.png">
	<script async src="/wp-content/themes/nycbuildsbio/coming-soon/js/src.js"></script>
</head>
<body class="fonts_not_loaded"">
	<!-- <div class="top_logo_container">
		<a href="http://www.standard-companies.com/">
			<img src='/wp-content/themes/nycbuildsbio/coming-soon/images/standard_companies_logo.png' alt="">
		</a>
	</div> -->
	<div class="main_content_container">
		<div class="logo_container">
			<img src='/wp-content/themes/nycbuildsbio/coming-soon/images/logo.png' alt="">
		</div>
		<div class="info_container container">
			<div>
				<p class="text header">
					<span class="green">Website </span><span class="blue">Coming Soon</span>
				</p>
				<div class="two_col_container">
					<p class="text grey subheader left_column">
						The City's Premier Organization for Real Estate, Design and Life Science Professionals
					</p>
					<p class="text grey_paragraph right_column">
						Launched in 2018, NYC Builds Bio+ aims to connect commercial life science opportunities to New York City's real estate development community through events, research, reports and educational programs.
						<br />
						<br />
						A membership-driven organization, NYC Builds Bio+ is a 501(c)(3) nonprofit organization dedicated to bringing New York City's real estate and life sciences community together, serving as the go-to place to find information about and assistance with growing, building and locating life science companies in New York City.
					</p>
				</div>
			</div>
		</div>
		<div class="footer_container">
			<p class="text blue footer_text">
				To contact us, please call <a href="tel:2124507300">212.450.7300</a> or email <a href="mailto:info@nycbuildsbio.org">info@nycbuildsbio.org</a>
			</p>
		</div>
	</div>
</body>
</html>