<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '88cc43f0f4a0c689b23d2963a693a273d55263466f59f6d7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oZO}yX^Nil,s|/fGPjF-$_)KJ-zH?}v/7*X.&_rBS8KmH=9Oipo^J0h&hQHw5n|1');
define('SECURE_AUTH_KEY',  '?]xqlZe?~F/)c |Xw7xc?<1)cMDx0aFM_`^J9U:@WpE){@JU&CUAfqE(.x(zE`<l');
define('LOGGED_IN_KEY',    'yR*3{%X8VoSn83/k^<AM{$S)`Q>KfJo1iCUtaf!guA)!CS|tqcPQg[U];8^mG^[F');
define('NONCE_KEY',        '=d&w[Bg?,G!Z>si/3MA/n69R9]6eIkHB%$M-)=X!qD<}*] (Hba}xK!)LcJ)!!zG');
define('AUTH_SALT',        'Ghd %)eDxAQ886`Z7z0FkcPOo~X3?v3zqXsgwruHr13}a2pV_4Wx&$4VK,*o51MS');
define('SECURE_AUTH_SALT', 'JRDsdmA7<!bv>kyAq/h $E2[KNV<Ie %5Z{g@.@SDR_c#VOE#~mb1/0^@}-3GhZ;');
define('LOGGED_IN_SALT',   'fz71&#Y4Eg_Gf2+/q }FrkBQ%ft<HQL1aO0uBf22#Zgo:i.w#*-Atp#J$>:}rl^p');
define('NONCE_SALT',       'BzI5 &{Y~N.22X,WX5hSroFS|S:GBd~ee2Y5T:Bes7r~bUMrt2esOAFS#k1}Slrq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

