User-agent: *

Allow: /
Allow: /*.php$
Allow: /about

Disallow: /wp-content/plugins/
Disallow: /readme.html
Disallow: /wp-login.php
Disallow: /wp-content/uploads/2018/11/2018_NYC_Build_Bio_Member_Memo_v19.pdf
Disallow: /robots.txt
Disallow: /sample-page/

Sitemap: http://www.nycbuildsbio.org/sitemap.xml